import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { ActivityIndicator, View, StyleSheet, StatusBar } from 'react-native';
import TabNavigation from '~/navigation/TabNavigation';
import SignIn from '~/screens/SignIn';
// import Login from '~/screens/Login';
import NewUserStack from '~/screens/TabUsers/NewUserStack';
import { inject, Provider } from 'mobx-react';
import { createStackNavigator } from '@react-navigation/stack';
import NewRequestStack from '~/screens/TabRequset/NewRequestStack';
import RequestStack from '~/screens/TabRequset/RequestStack';
import SelectUser from '~/screens/TabUsers/SelectUser';
import SelectPicker from '~/screens/TabPickers/SelectPicker';
import { navigationRef, isReadyRef } from '~/navigation/RootNavigation';
import { observer } from 'mobx-react';
import storeProviderHOC from '~/components/storeProviderHOC';
import NewPickerStack from '~/screens/TabPickers/NewPickerStack';
import ChoosePhotoStack from '~/screens/TabRequset/ChoosePhotoStack';
import About from '~/screens/About/About';






const RootStack = createStackNavigator();



const App = (props) => {

  const { isSignedIn, loading } = props.appStore;


  React.useEffect(() => {
    return () => {
      isReadyRef.current = false;
    };
  }, []);

  return (
    <>
      <StatusBar barStyle="dark-content" hidden={false} backgroundColor="white" translucent={true} />
      <NavigationContainer ref={navigationRef}
        onReady={() => {
          isReadyRef.current = true;
        }}>
        {loading ?
          <View style={[styles.container, styles.horizontal]}><ActivityIndicator size="large" color="#00ba88" /></View> :
          !isSignedIn.get() ?
            <RootStack.Navigator>
              <RootStack.Screen name="SignIn" component={SignIn} options={{ headerShown: false, title: "Вход" }} />
            </RootStack.Navigator>
            : <RootStack.Navigator>
              <RootStack.Screen name="TabNavigation" component={TabNavigation} options={{ headerShown: false }} />
              <RootStack.Screen name="NewUserStack" component={NewUserStack} options={{ headerTitle: 'Новый пользователь', headerTitleAlign: 'center' }} />
              <RootStack.Screen
                name="NewRequestStack"
                component={NewRequestStack}
                options={({ route }) => {
                  return {
                    headerBackTitle: () => null,
                    headerTitleAlign: 'center',
                    title: "Новая заявка"
                  }
                }}
              />
              <RootStack.Screen
                name="RequestStack"
                component={RequestStack}
                options={({ route }) => {
                  return {
                    headerBackTitle: () => null,
                    headerTitleAlign: 'left',
                    headerStyle: {
                      elevation: 0, // remove shadow on Android
                      shadowOpacity: 0, // remove shadow on iOS
                    },
                    title: route.params.post
                  }
                }}
              />
              <RootStack.Screen
                name="ChoosePhotoStack"
                component={ChoosePhotoStack}
                options={({ route }) => {
                  return {
                    headerBackTitle: () => null,
                    headerBackTitleVisible: false,
                    headerTitleAlign: 'left',
                    title: ''
                  }
                }}
              />
              <RootStack.Screen
                name="NewPickerStack"
                component={NewPickerStack}
                options={({ route }) => {
                  return {
                    headerBackTitle: () => null,
                    headerTitleAlign: 'center',
                    title: "Новый подборщик"
                  }
                }}
              />
              <RootStack.Screen
                name="SelectUser"
                component={SelectUser}
                options={({ route }) => {
                  return {
                    title: 'Выбор оценщика',
                    headerBackTitle: () => null,
                  }
                }}
              />
              <RootStack.Screen
                name="SelectPicker"
                component={SelectPicker}
                options={({ route }) => {
                  return {
                    headerBackTitle: () => null,
                    title: route.params.post
                  }
                }}
              />
              <RootStack.Screen
                name="AboutApp"
                component={About}
                options={({ route }) => {
                  return {
                    title: 'О приложении',
                    headerBackTitle: () => null,
                  }
                }}
              />
            </RootStack.Navigator>


        }
      </NavigationContainer>
    </>
  )
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});


export default storeProviderHOC(inject("appStore")(observer(App)), Provider);