const createExpoWebpackConfigAsync = require('@expo/webpack-config');
const path = require("path");
const webpack = require("webpack")

// Main const. Feel free to change it
const PATHS = {
    src: path.join(__dirname, "./src")
};

// Expo CLI will await this method so you can optionally return a promise.
module.exports = async function (env, argv) {
    const config = await createExpoWebpackConfigAsync(env, argv);
    // If you want to add a new alias to the config.
    config.resolve.alias['moduleA'] = 'moduleB';

    // Maybe you want to turn off compression in dev mode.
    if (config.mode === 'development') {
        config.devServer.compress = false;
    }

    if (config.mode === 'development') {
        config.devServer.proxy = {
            '/**': {
                target: {
                    host: 'example.com',
                    protocol: 'https:',
                    port: 443,
                },
                secure: false,
                changeOrigin: true,
                logLevel: 'info',
            },
        };
    }

    const babelLoaderConfiguration = {
        test: /\.(js|jsx)$/,
        use: {
            loader: 'babel-loader',
            options: {
                cacheDirectory: true,
                // The 'metro-react-native-babel-preset' preset is recommended to match React Native's packager
                presets: ['module:metro-react-native-babel-preset'],
                // Re-write paths to import only the modules needed by the app
                plugins: ['react-native-web']
            }
        }
    };

    const imageLoaderConfiguration = {
        test: /\.(gif|jpe?g|png|svg)$/,
        use: {
            loader: 'url-loader',
            options: {
                name: '[name].[ext]'
            }
        }
    };

    const fileLoaderConfiguretion = {
        test: /\.(eot|svg|ttf|woff|woff2|otf)$/,
        use: [
            {
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    limit: 10000,
                    mimetype: 'application/font-woff',
                },
            },
        ],
    }

    const svgLoaderConfiguration = {
        test: /\.inline.svg$/,
        loader: 'svg-react-loader'
    }

    // Or prevent minimizing the bundle when you build.
    if (config.mode === 'production') {
        config.optimization.minimize = false;
    }

    config.module = {
        rules: [
            babelLoaderConfiguration,
            imageLoaderConfiguration,
            fileLoaderConfiguretion,
            svgLoaderConfiguration
        ]
    }

    config.resolve = {
        alias: {
            "~": PATHS.src,
            'react-native$': 'react-native-web'
        },
        extensions: ['.web.js', '.js']
    }

    config.devtool = 'inline-source-map'
    // Finally return the new config for the CLI to use.
    return config;
};