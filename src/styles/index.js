import { StyleSheet } from 'react-native';
import { DeviceConst } from './../constants/constants';

const padding = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

export const COLORS = {
  mainBg: '#FFFFFF',
  mainGreen: '#00BA88',
  mainRed: '#FF3D71',
  mainViolet: '#BE38FD',
  mainYellow: '#FFAA00',
  mainBlue: '#5353FF',
  default: '#A9ABC3',
  lightGreen: '#EBFCF6',
  border: '#C5CEE0',
  lightBlue: '#EBF7FF',
  defaultFilter: '#F5F6FA',
  inworkStatus: '#00BA88',
  newStatus: '#5353FF',
  expiredStatus: '#FFAA00',
  buyingStatus: '#BE38FD',
  rejectionStatus: '#FF3D71'


};

export const STYLES = StyleSheet.create({
  indicator: {
    flex: 1,
    justifyContent: "center",
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  barStyle: {
    paddingLeft: padding,
    paddingRight: padding,
    marginLeft: -8,
    flexDirection: 'row',
    // flexWrap: 'wrap',
  },
});