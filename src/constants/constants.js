import { Dimensions, PixelRatio, Platform } from 'react-native';
const { width, height, scale } = Dimensions.get('window');

const API = 'https://appraiser.mediatver.ru/api/';

export const API_URL = {

    USERS: `${API}user?fields=id,email,roles,name,phone,online&sort=name`,
    USER: `${API}user`,
    USER_CREATE: `${API}user/create`,
    USER_CARD: `${API}card?sort=-created_at&filter[executor_id]=`,
    CARD_CREATE: `${API}card/create`,
    MESSAGE_CREATE: `${API}card-message/create`,
    PICKER_CREATE: `${API}picker/create`,
    CARD_CREATE: `${API}card/create`,
    STATUS: `${API}card/status`,
    CARD: `${API}card`,
    CITIES: `${API}city`,
    PICKER: `${API}picker?sort=-created_at`,
    STAT_CITY: `${API}site/stat-city?sort=title`,
    STAT_PICKERS: `${API}site/stat-executor?sort=title`,
    CARD_SORT: `${API}card?fields=id,title,author_name,item.phone,created_at,urgency,status_id,expired&sort=-created_at`,
    CARD_STATUS: `${API}card-status`,
    CHANGE_STATUS: `${API}card/`,
    SIGN_IN: `${API}site/login`,
    ROLES: `${API}site/roles`,
    STATUS: `${API}card/status`,
    CARDS_COUNT: `${API}card?only=status_counts`,
    PROFILE: `${API}site/profile`,
    MESSAGES: `${API}card-message`,
    UPLOAD_PHOTO: `${API}card/upload-photo`,
    GET_TITLE: `${API}card/get-title`,
    UPLOAD_AUTOTEKA: `${API}card/upload-autoteka`
};


export const BASE_OPTIONS = {

};

export const DeviceConst = {
    screenWidth: width,
    screenHeight: height,
    screenScale: scale,
    pixelRatio: PixelRatio,
    isIos: Platform.OS === 'ios',
    isAndroid: Platform.OS === 'android',
    platform: Platform.OS
};

