import AppStore from "./AppStore";
import FetchStore from "./FetchStore";
import UserStore from "./UserStore";
import UsersTabStore from "./UsersTabStore";
import DataStore from "./DataStore";
import OverallStore from './OverallStore';
import PickerStore from './PickerStore'
import DashboardsStore from './DashboardsStrore';
import NotifficationStore from './NotifficationStore';
import { observe } from 'mobx';
export default class RootStore {

    constructor() {
        this.initStore()


        observe(this.appStore.isSignedIn, change => {
            if (change.newValue) {
                this.appStore.getPermissions();
                this.loadData()
            }
        })
    }

    initStore() {
        this.appStore = new AppStore(this);
        this.userStore = new UserStore(this);
        this.fetchStore = new FetchStore(this);
        this.overallStore = new OverallStore(this);
        this.usersTabStore = new UsersTabStore(this);
        this.dataStore = new DataStore(this);
        this.pickerStore = new PickerStore(this);
        this.dashboardsStore = new DashboardsStore(this);
        this.notifficationStore = new NotifficationStore(this);
    }


    loadData() {
        this.fetchStore.getProfile();
        this.fetchStore.getCitiesList();
        this.fetchStore.getUsersListData();
        this.fetchStore.getCardsCount();
        this.fetchStore.getStatusList();
        this.fetchStore.getCardsListData();
    }
}