import {
    observable, makeObservable, action, computed,
} from 'mobx';

export default class NotifficationStore {
    @observable notiffication = [];
    @observable currentPage = 1;
    @observable pagesCount = null;

    constructor(rootStore) {
        makeObservable(this);
        this.rootStore = rootStore;

    }

    @action setNotiffication(data) {
        this.notiffication = data;
    }

    @action loadMoreNotiffication(data) {
        Array.prototype.push.apply(this.notiffication, data);
    }
}