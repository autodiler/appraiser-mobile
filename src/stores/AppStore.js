
import { action, observable, makeObservable } from 'mobx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { API_URL } from '../constants/constants';
import { Platform } from 'react-native';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';

const status = async (response) => {

  if (response.status == 200) {
    return Promise.resolve(response)
  }
  if (response.status == 201) {
    return Promise.resolve(response)
  }
  if (response.status == 422) {
    const json = await response.json();
    return Promise.reject(new Error(JSON.stringify(json)));
  }
  return Promise.reject(new Error(response.statusText))
}

const json = (response) => {

  return response.json()
}


export default class AppStore {

  @observable loading = true;
  @observable dataLoading = false;
  @observable permissions = {};
  @observable expoPushToken = null;

  @observable hasAuthError = false;

  @observable notification = null;

  constructor(rootStore) {
    makeObservable(this);

    this.isSignedIn = observable.box(false);
    this.rootStore = rootStore;


  }

  @action setNotification(notify) {
    this.notification = notify;
  }

  @action setDataLoading(val) {
    this.dataLoading = val;
  }

  @action setSignIn(val) {
    this.isSignedIn.set(val);
  }

  @action setLoading(val) {
    this.loading = val;
  }

  @action setPermissions(data) {
    this.permissions = data;
  }

  @action setExpoPushToken = (token) => {
    this.expoPushToken = token;
  }

  @action setErrorStatus = (val) => {
    this.hasAuthError = val;
  }

  async logout() {
    await AsyncStorage.clear();
    this.setExpoPushToken(null);
    this.initAuthToken();
  }

  async getInitToken(contentType) {

    const authData = await AsyncStorage.getItem('authentication_data');
    console.log(authData)

    if (authData !== null) {

      return {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${JSON.parse(authData).authToken}`,
          'Content-Type': !contentType ? 'application/json' : contentType
        }
      }
    }
    return null
  }

  async initAuthToken() {

    const headers = await this.getInitToken();

    if (headers !== null) {
      fetch(API_URL.CARD, headers).then(status)
        .then(json)
        .then((data) => {
          this.setLoading(false);
          this.setSignIn(true);
        })
        .catch((e) => {
          this.setLoading(false);
          this.setSignIn(false);
        });
    } else {
      this.setLoading(false);
      this.setSignIn(false);
    }
  }

  getPermissions = async () => {
    const headers = await this.getInitToken();
    if (headers !== null) {
      fetch(`${API_URL.PROFILE}`, headers,)
        .then((response) => {
          return response.json()
        })
        .then((data) => {
          this.setPermissions(data.permissions);
        })
        .catch((e) => {
          console.log(e)
        });
    }
  }

  async handleSignIn(data) {

    await AsyncStorage.setItem('authentication_data', JSON.stringify({
      authToken: data.token
    }));

    this.setLoading(false);
    this.setSignIn(true);
  }


  async signIn(values) {

    this.setLoading(true);

    if (!this.expoPushToken && !Platform.OS == 'web') {
      await this.registerForPushNotificationsAsync().then(token => {
        this.setExpoPushToken(token)
      });
    }


    values['expo_push_token'] = this.expoPushToken;
    values['os'] = Platform.OS;
    console.log(values);
    fetch(API_URL.SIGN_IN, {
      method: 'POST',
      mode: 'cors',
      //credentials:'include',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(values)
    }).then(status)
      .then(json)
      .then((data) => {
        this.handleSignIn(data);
      })
      .catch((e) => {
        this.setErrorStatus(true);
        this.setLoading(false);
      });;


  }


  async registerForPushNotificationsAsync() {

    let token;
    if (!this.expoPushToken) {
      if (Constants.isDevice) {
        const { status: existingStatus } = await Notifications.getPermissionsAsync();
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
          const { status } = await Notifications.requestPermissionsAsync();

          finalStatus = status;
        }
        if (finalStatus !== 'granted') {
          alert('Failed to get push token for push notification!');
          return;
        }
        token = (await Notifications.getExpoPushTokenAsync()).data;
      } else {
        alert('Must use physical device for Push Notifications');
      }

      if (Platform.OS === 'android') {
        Notifications.setNotificationChannelAsync('default', {
          name: 'default',
          importance: Notifications.AndroidImportance.MAX,
          vibrationPattern: [0, 250, 250, 250],
          lightColor: '#FF231F7C',
        });
      }
    }
    return token;
  }

  can = (rule) => {
    return this.permissions.hasOwnProperty(rule)
  }


}
