import {
    observable, action, computed, makeObservable
} from 'mobx';

export default class OverallStore {
    @observable.deep citiesListForNew = [];
    @observable.deep citiesListForPickers = [];
    @observable.deep citiesListForPickersNew = [];
    @observable.deep citiesList = [];
    @observable.deep currentCity='';

    constructor(rootStore) {
        makeObservable(this);
        this.rootStore = rootStore;
  
    }

    @action setCitiesList(data) {
        let arr = data.map((item) => {
            item.checked = false;
            return item;
        })
        this.citiesListForNew = arr;
        this.citiesListForPickersNew = arr;

        const firstItem = {
            "checked": true,
            "id": 0,
            "title": "Все города"
        };
        arr.unshift(firstItem);
       
        this.citiesList=arr;
        this.citiesListForPickers=arr;
                
    }

    @action.bound setTownCheckedCard({ item, index }) {
        let arr = this.citiesList.map((town) => {
            if (town.id == item.id) {
                town.checked = true
            } else {
                town.checked = false
            }
            return town;
        })

        this.citiesList.replace(arr);

    }
    
    @action.bound setTownCheckedPickers({ item, index }) {
        let arr = this.citiesListForPickers.map((town) => {
            if (town.id == item.id) {
                town.checked = true
            } else {
                town.checked = false
            }
            return town;
        })

        this.citiesListForPickers.replace(arr);
    }  
    
    @action.bound setTownCheckedPickersNew({ item, index }) {
        let arr = this.citiesListForPickersNew.map((town) => {
            if (town.id == item.id) {
                town.checked = true
            } else {
                town.checked = false
            }
            return town;
        })

        this.citiesListForPickersNew.replace(arr);
    }   

    @action.bound setTownCheckedNew({ item, index }) {
        let arr = this.citiesListForNew.map((town) => {
            if (town.id == item.id) {
                town.checked = true
            } else {
                town.checked = false
            }
            return town;
        })

        this.citiesListForNew.replace(arr);

    }


    @computed get getCitiesListCard() {
        return this.citiesList;
        
    }

    @computed get getCitiesListNew() {
        return this.citiesListForNew;
        
    }
    @computed get getCitiesListPickers() {
        return this.citiesListForPickers;
        
    }
    @computed get getCitiesListPickersNew() {
        return this.citiesListForPickersNew;
        
    }
    

    @computed get getActiveTownCard() {
        let city='';
        let arr = this.citiesList.map((town) => {
            if (town.checked) {
                city = town.title;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveIdTownCard() {
        let city;
        let arr = this.citiesList.map((town) => {
            if (town.checked) {
                city = town.id;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveTownPickers() {
        let city='';
        let arr = this.citiesListForPickers.map((town) => {
            if (town.checked) {
                city = town.title;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveIdTownPickers() {
        let city;
        let arr = this.citiesListForPickers.map((town) => {
            if (town.checked) {
                city = town.id;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveTownPickersNew() {
        let city='';
        let arr = this.citiesListForPickersNew.map((town) => {
            if (town.checked) {
                city = town.title;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveIdTownPickersNew() {
        let city;
        let arr = this.citiesListForPickersNew.map((town) => {
            if (town.checked) {
                city = town.id;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveTownNew() {
        let city='';
        let arr = this.citiesListForNew.map((town) => {
            if (town.checked) {
                city = town.title;
                return ;
            }             
        })
        return city;
    }
    @computed get getActiveIdTownNew() {
        let city;
        let arr = this.citiesListForNew.map((town) => {
            if (town.checked) {
                city = town.id;
                return ;
            }             
        })
        return city;
    }

}