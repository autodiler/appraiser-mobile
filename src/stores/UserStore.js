import {
  observable, action, computed, makeObservable
} from 'mobx';

export default class UserStore {

  @observable login = '+79000000000';

  @observable password = '12345678';

  @observable profile = null;

  constructor(rootStore) {
    makeObservable(this);
    this.rootStore = rootStore;
  }

  @action.bound setProfile(profile) {
    this.profile = profile;
  }

  @action.bound setLogin(val) {
    this.login = val;
  }

  @action.bound setPassword(val) {
    this.password = val;
  }

  @computed get getLogin() {
    return this.login;
  }

  @computed get getPassword() {
    return this.password;
  }
}
