import {
    observable, makeObservable, action, computed,
} from 'mobx';

export default class DashboardsStore {
    @observable.deep citiesList = [];

    @observable dashboardsFilter = [
        { title: "Общая", active: true, value: 'Dashboards', count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: null, filter_name: null, permissions: "Э-СтатистикаОбщая" },
        { title: "Города", active: false, value: 'DashboardsTown', count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: null, filter_name: 'STAT_CITY', permissions: "Э-СтатистикаГорода" },
        { title: "Оценщики", active: false, value: 'DashboardsPickers', count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: null, filter_name: 'STAT_PICKERS', permissions: "Э-СтатистикаОценщики" },
    ]

    @observable cardsCount = {
        new: null,
        inwork: null,
        purchased: null,
        rejection: null,
        waiting: null
    }

    @observable stat_name = '';

    @observable statistic = [];
    @observable listLoading = false;
    @observable statPage = 1;
    @observable statisticPageCount = null;

    constructor(rootStore) {
        makeObservable(this);
        this.rootStore = rootStore;

    }

    @action.bound setCardListCount(data) {
        this.cardsCount = data;
    }

    @action.bound setStatistic(data) {
        let newStatisctic = this.statistic.concat(data.slice());
        this.statistic.replace(newStatisctic);
    }

    @action.bound setStatisticPageCount(val) {
        this.statisticPageCount = val;
    }

    @action.bound clearStatistic() {
        this.statistic.clear();
    }

    @action.bound setStatisticPage(val) {
        this.statPage = val ? val : this.statPage + this.statPage;
    }

    @computed get getDashboardsFilter() {
        let filters = [];
        this.dashboardsFilter.forEach((item) => {
            if (this.rootStore.appStore.can(item.permissions)) {
                filters.push(item)
            }
        })
   
        if(filters.length == 1){
            return [];
        }
        return filters;
    }

    @action setDashboardsFilter(filter, index) {
        let arr = this.dashboardsFilter.map((item, id) => {
            if (index == id) {
                item.active = true;
            } else {
                item.active = false
            }
            return item;
        })

        this.dashboardsFilter.replace(arr);
    }

    @computed get getActiveFilter() {
        let filter = this.dashboardsFilter.filter((item) => {
            return item.active
        });

        return filter[0].filter_name;
    }
}