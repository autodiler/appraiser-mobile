import {
    observable, action, computed, makeObservable,
} from 'mobx';
import { reaction } from 'mobx';



export default class UsersTabStore {

    @observable userListData = [];
    @observable rolesList = [];
    @observable pageCount = null;
    @observable page = 1;

    @observable createUserStatus = {
        status: ''
    };

    @observable filter = {
        status: null,
        name: ''
    }

    @observable filters = [
        { title: "Все", active: true, value: null, count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: "all" },
        { title: "Онлайн", active: false, value: true, count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: "online" },
        { title: "Оффлайн", active: false, value: false, count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: "offline" }
    ]

    @observable cardFilters = [
        { title: "Все", active: true, value: null, count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: "all" }
    ]

    @observable refreshing = false;
    @observable loading = false;

    @observable selectedUserID = null;

    @observable selectedUserFilterStatus='';


    @observable userCards = [];


    constructor(rootStore) {
        makeObservable(this);
        this.rootStore = rootStore;


        reaction(
            () => this.filter.status,
            () => {
                this.refreshList();
            }
        )

        reaction(
            () => this.selectedUserFilterStatus,
            () => {
                this.refreshUserCardList();
            }
        )

        reaction(
            () => this.filter.name,
            () => {
                if (this.timer) {
                    clearTimeout(this.timer);
                }
                this.timer = setTimeout(() => {
                    this.refreshList();
                }, 700)
            }
        )
    }

    @computed get getUserCards() {
        return this.userCards;
    }

    @action setStatusList = (data) => {
        this.statusList = data;
        if (this.cardFilters.length == 1) {
            data.forEach(item => {
                let newItem = {
                    title: item.title,
                    active: false,
                    value: item.code,
                    count: null,
                    activeBg: item.color1,
                    unactiveBg: item.color2,
                    state: item.code
                };
                this.cardFilters.push(newItem);
            });
        }
    }

    @action setUserCounts(counts) {
        this.filters[0].count = counts.all;
        this.filters[1].count = counts.online;
        this.filters[2].count = counts.offline;
    }

    @action setUserListData(data, counts) {
        this.setUserCounts(counts);
        let newList = this.userListData.concat(data.slice());
        this.userListData.replace(newList);
    }


    @action setUserCardsCounts(counts) {
        if (this.cardFilters) {
            this.cardFilters[0].count = 0;
            for (let key in counts) {
                let ind = this.cardFilters.findIndex(item => {
                    if (item.state === key) return true
                })
                if (ind != -1) this.cardFilters[ind].count = counts[key];
                this.cardFilters[0].count += +counts[key];
            }

        }
    }

    @action setUserCards(data, counts) {
        this.setUserCardsCounts(counts)
        this.userCards.replace(data);
    }

    @action clearUserCards() {
        this.userCards.clear();
    }

    @action setRolesList(data) {

        let newList = Object.values(data).map((item) => {
            let fItem = {
                title: item.description,
                key: item.name,
            };
            return fItem;
        });

        this.rolesList.replace(newList);
    }

    @action setSelectedUserID(val) {
        this.selectedUserID = val;
    }

    @action clearUserListData() {
        this.setDefaultPage();
        this.userListData.clear();
    }

    getRoleByName = (roles) => {
        let roleStr = [];

        let arr = roles = roles.replace(/\s+/g, '').split(',');

        for (var i = 0; i < this.rolesList.length; i++) {
            for (var c = 0; c < arr.length; c++) {
                if (arr[c] == this.rolesList[i].title) {
                    roleStr.push(this.rolesList[i].key)
                }
            }
        }

        return roleStr.join(',')
    }

    @action setRoleItem(data) {
        this.rolesList.replace(data)
    }

    @action.bound setPage() {
        this.page = this.page + 1;
    }

    @action setDefaultPage() {
        this.page = 1;
    }

    @action.bound setRefreshing(val) {
        this.refreshing = val;
    }

    @action refreshList = () => {
        const { fetchStore } = this.rootStore;
        this.setRefreshing(true);
        this.clearUserListData();
        fetchStore.getUsersListData('refresh');
    }

    @action refreshUserCardList = () => {
        const { fetchStore } = this.rootStore;
        fetchStore.getUserCardsListData(this.selectedUserID);
    }


    @action setLoading = (val) => {
        this.loading = val;
    }

    @action loadMore = () => {
        const { fetchStore } = this.rootStore;
        this.setLoading(true);
        this.setPage();
        fetchStore.getUsersListData('load');
    }

    @action setPageCount(val) {
        this.pageCount = val;
    }

    @action setCreateUserStatus(val) {
        this.createUserStatus.status = val;
    }

    @action.bound setActiveFilter(idx) {
        this.filters.forEach((item, index) => {
            index == idx ? item.active = true : item.active = false;
        })
    }

    @action.bound setFilterStatus(val, index) {
        if (val != this.filter.status) {
            this.filter.status = val;
        }
        this.setActiveFilter(index);
    }

    @action.bound setFilterByUser(val, index) {
       
        this.cardFilters.forEach((item, idx) => {
            idx == index ? item.active = true : item.active = false;
        })

        this.setselectedUserFilterStatus(val);
    }

    @action.bound setselectedUserFilterStatus(val) {
        this.selectedUserFilterStatus=val;
    }

    @action.bound setSearchString(val) {
        this.filter.name = val;
        if (this.timer != null) {
            clearTimeout(this.timer)
        } else {
            this.timer = setTimeout(() => { this.refreshList() }, 1000);
        }
    }

    @computed get getFilterByStatus() {
        let { status } = this.filter;
        let string = "&filter[online]=";
        if (status == null) {
            return ''
        } else if (status == true) {
            return `${string + status}`;
        } else if (status == false) {
            return `${string + status}`;
        }
    }

    @computed get getFilterByName() {
        const { name } = this.filter;
        let string = name.length == 0 ? '' : `&filter[name]=${name}`
        return string;
    }

    @computed get getSelectedUserFilterStatus() {
        let data='all';
        this.cardFilters.map((item) => {
            if(item.active){
                data = item.state;        
            }            
        })   
        return data;  
    }
    

    @computed get getFilterString() {
        return `${this.getFilterByStatus + this.getFilterByName}`
    }

    @computed get getUserListData() {
        let data = this.userListData.map((item) => {
            let role = item.roles.map((role) => {
                return role.description;
            })
            return { userId: item.id, name: item.name, phone: item.phone, role: role.join(', '), status: item.online ? 'online' : 'offline' }
        })
        return data;
    }

    @computed get getRolesList() {
        return this.rolesList;
    }


}

