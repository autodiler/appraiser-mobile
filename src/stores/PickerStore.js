import {
    observable, action, computed, makeObservable, reaction
} from 'mobx';

export default class PickerStore {
    @observable pickersList = [];
    
    @observable filter = {
        status: null,
        name: ''
    }

    constructor(rootStore){
        makeObservable(this);
        this.rootStore = rootStore;

        reaction(
            () => this.filter.name,
            () => {
                if (this.timer) {
                    clearTimeout(this.timer);
                }
                this.timer = setTimeout(() => {
                    this.refreshList();
                }, 700)
            }
        )
    }


    @action setPickersList(data) {
        this.pickersList=data;
    }

    @action refreshList = () => {
        const { fetchStore } = this.rootStore;
        this.clearPickerListData();
        fetchStore.getPickersList();
    }

    @action clearPickerListData() {
        this.pickersList.clear();
    }

    @computed get getPickersList() {
        return this.pickersList;
    }

    @action.bound setSearchString(val) {
        this.filter.name = val;
        if (this.timer != null) {
            clearTimeout(this.timer)
        } else {
            this.timer = setTimeout(() => { this.refreshList() }, 1000);
        }
    }

    @computed get getFilterByName() {
        const { name } = this.filter;
        let string = name.length == 0 ? '' : `&search=${name}`
        return string;
    }


}