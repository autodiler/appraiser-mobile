import {
    observable, action, computed, makeObservable, reaction
} from 'mobx';
import { COLORS } from '~/styles';

export default class DataStore {

    @observable profile = null;

    @observable cardsListData = [];
    @observable ownID = null;
    @observable ownName = null;
    @observable ownPhone = null;
    @observable cardData = {};
    @observable page = 1;
    @observable pageCount = null;
    @observable refreshing = false;
    @observable loading = false;

    @observable.deep messageList = [];
    @observable filter = {
        status: null,
        name: ''
    }
    @observable expandable = false;
    @observable filterCityId = null;

    @observable createCardStatus = {
        status: ''
    };

    @observable errors = [];

    @observable newCardTitle = '';

    @observable filters = [
        { title: "Все", active: true, value: null, count: null, activeBg: "#00BA88", unactiveBg: "#F5F6FA", state: "all" }
    ]

    @observable filterCardList = [

        //{ title: 'Просрочено', key: 'qwer', checked: false },
        { title: 'Без исполнителя', key: 'qwer1', checked: false },
        { title: 'Без автотеки', key: 'qwer2', checked: false },
        { title: 'Без фотографий', key: 'qwer3', checked: false },
        { title: 'Созданные мной', key: 'qwer4', checked: false }
    ];

    @observable cardStatusList = [
        { title: 'Новая', key: 'new', checked: false, permission: "Э-Новая-Новая" },
        { title: 'В работе', key: 'inwork', checked: false, permission: "Э-Новая-ВРаботе" },
        { title: 'Отказ', key: 'rejection', checked: false, permission: "Э-Новая-Отказ" },
        { title: 'Выкуплен', key: 'purchased', checked: false, permission: "Э-Новая-Выкуплен" },
        { title: 'В ожидании', key: 'waiting', checked: false, permission: "Э-Новая-ВОжидании" }
    ];



    @observable filterCount = 0;

    @observable statusList = [];

    constructor(rootStore) {
        makeObservable(this);
        this.rootStore = rootStore;
        this.new_card = null;

        reaction(
            () => this.filter.status,
            () => {
                this.refreshList();
            }
        )

        reaction(
            () => this.filterCount,
            () => {
                this.refreshList();
            }
        )

        reaction(
            () => this.filterCityId,
            () => {
                this.refreshList();
            }
        )



    }

    @action setFilterCardChecked(title) {
        let arr = this.filterCardList.map((item) => {
            if (item.title == title) {
                item.checked = true
            }
            return item;
        })

        this.filterCardList.replace(arr);
        this.setFilterCount();

    }

    @action setExpandable() {
        this.expandable = !this.expandable;
    }

    @action setCardsListData(data, counts) {
        this.setUserCounts(counts);
        let newList = this.cardsListData.concat(data.slice());
        this.cardsListData.replace(newList);
        this.setLoading(false);
        this.setRefreshing(false);
    }

    @action setCardData(data) {
        this.cardData = data;
    }

    @action setNewCardTitle(data) {
        this.newCardTitle = '';
        this.newCardTitle = data.title;
    }

    @action setStatusList(data) {
        this.statusList = data;
        if (this.filters.length == 1) data.map(item => {
            let newItem = {
                title: item.title,
                active: false,
                value: item.code,
                count: null,
                activeBg: item.color1,
                unactiveBg: item.color2,
                state: item.code
            };
            this.filters.push(newItem);
        })
    }

    @action setErrors(data) {
        this.errors.replace(data);
    }

    @action setPageCount(val) {
        this.pageCount = val;
    }

    @action.bound setPage() {
        this.page = this.page + 1;
    }

    @action.bound setRefreshing(val) {
        this.refreshing = val;
    }

    @action setFilterCityId(id) {
        this.filterCityId = id;
    }

    @action setLoading = (val) => {
        this.loading = val;

    }

    @action loadMore = () => {
        const { fetchStore } = this.rootStore;
        this.setLoading(true);
        this.setPage();
        fetchStore.getCardsListData('load');
    }


    @action refreshList = () => {
        const { fetchStore } = this.rootStore;
        this.setRefreshing(true);
        this.clearCardsListData();
        fetchStore.getCardsListData('refresh');
    }

    @action refreshCard = (itemId, callback) => {
        const { fetchStore } = this.rootStore;
        this.clearCardData();
        fetchStore.getCardData(itemId, callback);
    }

    @action.bound setActiveFilter(idx) {
        this.filters.forEach((item, index) => {
            index == idx ? item.active = true : item.active = false;
        })
    }



    @action setProfile(data) {
        const { setProfile } = this.rootStore.userStore;

        this.ownID = data.id;
        this.ownName = data.name;
        this.ownPhone = data.phone;
        setProfile(data);
    }


    @action setFilterCount() {
        this.filterCount = 0;
        this.filterCardList.map((item) => {
            if (item.checked) {
                this.filterCount += 1;
            }
        })
    }

    @action.bound setMessages(data) {
        this.clearMessageList();
        data.map(item => {
            let newItem = {
                _id: item.id,
                text: item.message,
                createdAt: new Date(item.created_at * 1000).getTime(),
                user: {
                    _id: item.author_id,
                    name: item.author_name,
                },
                statusColor: this.getColorByStatus(item.status_id)

            };
            this.messageList.push(newItem);
        })

    }

    @computed get getMessageList() {
        return this.messageList;
    }

    @computed get getNewCardTitle() {
        return this.newCardTitle;
    }

    @computed get getFilterCount() {
        return this.filterCount;
    }

    @computed get getCardStatusList() {
        let status = this.cardData.status_title;
        if (!status) {
            return [];
        }
        let tempStatus = [];
        if (this.cardData.status_transfer) {
            this.cardData.status_transfer.map(item => {
                tempStatus.push(item.to_status)
            })

        }
        let statuses = [];


        this.cardStatusList.forEach((item) => {

            if (tempStatus.includes(item.key)) statuses.push(item)

        });


        return statuses;
    }

    @computed get getActiveStatusList() {
        return this.citiesList;
    }

    @computed get getFilterCardList() {
        return this.filterCardList;
    }

    @computed get getOwnId() {
        return this.ownID;
    }

    @computed get getOwnName() {
        return this.ownName;
    }

    @computed get getActiveStatus() {
        let city = '';
        this.citiesList.map((town) => {
            if (town.checked) {
                city = town.title;
                return;
            }
        })
        return city;
    }
    @computed get getActiveIdStatus() {
        let city;
        let arr = this.citiesList.map((town) => {
            if (town.checked) {
                city = town.id;
                return;
            }
        })
        return city;
    }

    @computed get getFiltersIndex() {
        let obj = [];
        this.filters.map((item) => {
            obj.push(item.state + '');
        })
        return obj;
    }

    @action.bound setFilterStatus(val, index) {

        if (val != this.filter.status) {
            this.filter.status = val;
        }
        this.setActiveFilter(index);
    }

    @action clearMessageList() {
        this.messageList.clear();
    }

    @action clearCardsListData() {
        this.setDefaultPage();
        this.cardsListData.clear();
    }

    @action clearCardData() {
        this.cardData = {};
    }

    @action setDefaultPage() {
        this.page = 1;
    }

    @action setCreateCardStatus(val, new_card_data) {
        this.new_card = new_card_data;
        this.createCardStatus.status = val;
    }

    @action setUserCounts(counts) {
        if (this.filters) {
            this.filters[0].count = 0;
            for (let key in counts) {
                let ind = this.filters.findIndex(item => {
                    if (item.state === key) return true
                })
                if (ind != -1) this.filters[ind].count = counts[key];
                this.filters[0].count += +counts[key];
            }

        }
    }

    @computed get getFilterByStatus() {
        let { status } = this.filter;
        let string = "&filter[status_id]=";

        if (status == null) {
            return ''
        } else {
            return `${string + status}`;
        }
    }

    @computed get getFilterByName() {
        let string = '';
        //if (this.filterCardList[0].checked) { string += '&filter[expired]=1' }
        if (this.filterCardList[0].checked) { string += '&filter[no_executor]=true' }
        else if (this.filterCardList[1].checked) { string += '&filter[no_autoteka]=true' }
        else if (this.filterCardList[2].checked) { string += '&filter[no_photos]=true' }
        else if (this.filterCardList[3].checked) { string += `&filter[author_id]=${this.ownID}` }
        return string;
    }

    @computed get getFilterByCity() {
        let string = '';
        if (this.filterCityId != null && this.filterCityId != 0) string += `&filter[city_id]=${this.filterCityId}`;
        return string;
    }

    @computed get getCardData() {
        return this.cardData;
    }

    @computed get getFilterString() {
        return `${this.getFilterByStatus + this.getFilterByName + this.getFilterByCity}`
    }


    @computed get getCardsListData() {
        return this.cardsListData;
    }

    getFilter(name) {
        return this.filters.filter((item) => {
            return item.value == name;
        })[0]
    }

    getColorByStatus(status) {
        switch (status) {
            case 'new':
                //return COLORS.newStatus;
                return '#EBF7FF';
            case 'inwork':
                //return COLORS.inworkStatus;
                return '#EBFCF6';
            case 'waiting':
                //return COLORS.expiredStatus;
                return '#FFF9EB';
            case 'purchased':
                //return COLORS.buyingStatus;
                return '#F6EBFF';
            case 'rejection':
                // return COLORS.rejectionStatus; 
                return '#FFF0F4'
            default:
                //return 'black';
                return 'white';
        }
    }

    getStatusByCode = (code) => {
        let status = {};
        for (let i = 0; i < this.statusList.length; i++) {
            let item = this.statusList[i];
            if (item['code'] == code) {
                status.title = item.title;
                status.color = item.color1;
                break;
            }
        }

        return status;
    }

}

