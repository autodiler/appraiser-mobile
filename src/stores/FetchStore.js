
import * as Constants from '../constants/constants'

const status = async (response) => {
    if (response.status == 200) {
        return Promise.resolve(response)
    }
    if (response.status == 201) {
        return Promise.resolve(response)
    }
    if (response.status == 422) {
        const json = await response.json();
        return Promise.reject(new Error(JSON.stringify(json)));
    }
    return Promise.reject(new Error(response.statusText))
}

const json = (response) => {
    return response.json()
}

import AsyncStorage from '@react-native-async-storage/async-storage';
export default class FetchStore {

    constructor(rootStore) {
        this.rootStore = rootStore;

    }



    async getInitToken(contentType) {

        const authData = await AsyncStorage.getItem('authentication_data');
        console.log(authData)
        if (authData !== null) {
            return {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${JSON.parse(authData).authToken}`,
                    'Content-Type': !contentType ? 'application/json' : contentType
                }
            }
        }
        return null
    }


    getCardsCount = async () => {
        const { dashboardsStore } = this.rootStore;
        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.CARDS_COUNT}`, headers,)
                .then(status)
                .then((res) => {
                    return res
                })
                .then((res) => {
                    return res.json()
                })
                .then((data) => {
                    dashboardsStore.setCardListCount(data.status_counts);
                })
                .catch((e) => {
                    console.log(e)
                });
        }
    }

    getCardsListData = async (type) => {
        const { dataStore } = this.rootStore;
        const { page, getFilterString } = dataStore;

        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.CARD_SORT}&page=${page + getFilterString}`, { ...headers, method: 'GET'},)
                .then(status)
                .then((res) => {
                    return { headers: res.headers.map, res: res }
                })
                .then(({ headers, res }) => {
                    dataStore.setPageCount(res.headers.get('x-pagination-page-count'));
                    return res.json()
                })
                .then((data) => {

                    dataStore.setCardsListData(data.cards, data.status_counts);
                })
                .catch((e) => {
                    console.log(e)
                });
        }
    }


    getUsersListData = async (type) => {
        const { usersTabStore } = this.rootStore;
        const { page, getFilterString } = usersTabStore;

        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.USERS}&page=${page + getFilterString}`, headers,)
                .then(status)
                .then((res) => {
                    return { headers: res.headers.map, res: res }
                })
                .then(({ headers, res }) => {
                    usersTabStore.setPageCount(headers['x-pagination-page-count']);
                    return res.json()
                })
                .then((data) => {
                    usersTabStore.setUserListData(data.users, data.counts);
                    if (type == 'load') {
                        usersTabStore.setLoading(false)
                    }
                    if (type == 'refresh') {
                        usersTabStore.setRefreshing(false);
                    }
                })
                .catch((e) => {
                    console.log(e)
                });
        }

    }


    getUserCardsListData = async (userId) => {
        const { usersTabStore } = this.rootStore;
        const { getSelectedUserFilterStatus } = usersTabStore;
        let filt = getSelectedUserFilterStatus;
        let string = '';
        if (filt != 'all') {
            string = `&filter[status_id]=${filt}`;
        }
        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.USER_CARD}${userId}${string}`, headers,)
                .then(status)
                .then((res) => {
                    return { headers: res.headers.map, res: res }
                })
                .then(({ headers, res }) => {
                    return res.json();
                })
                .then((data) => {
                    usersTabStore.setUserCards(data.cards, data.status_counts);
                })
                .catch((e) => {
                    console.log(e)
                    usersTabStore.setRefreshing();
                });
        }
    }

    getPickersList = async () => {
        const { pickerStore } = this.rootStore;
        const { getFilterByName } = pickerStore;
        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.PICKER}${getFilterByName}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    pickerStore.setPickersList(data);
                })
                .catch((e) => {
                    console.log(e)
                });
        }

    }


    getRolesList = async () => {
        const { usersTabStore } = this.rootStore;

        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.ROLES}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    usersTabStore.setRolesList(data);

                })
                .catch((e) => {
                    console.log(e)
                });
        }
    }

    getCardData = async (itemId, callback) => {
        const { dataStore } = this.rootStore;
        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.CARD}/${itemId}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    dataStore.setCardData(data);
                    callback(data.title)
                })
                .catch((e) => {
                    console.log(e)
                    // appStore.setLoading(false);
                });
        }

    }

    getCitiesList = async () => {
        const { overallStore } = this.rootStore;

        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.CITIES}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    overallStore.setCitiesList(data);
                })
                .catch((e) => {
                    console.log(e)
                    // appStore.setLoading(false);
                });
        }
    }

    changeCard = async (card_id, data) => {
        const { dataStore } = this.rootStore;
        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL.CHANGE_STATUS}${card_id}`, {
                method: 'PUT',
                mode: 'cors',
                ...headers,
                body: JSON.stringify(data)
            })
                .then(status)
                .then(json)
                .then((data) => {
                    dataStore.setCardData(data)
                })
                .catch((e) => {
                    console.log(e)
                });
        }
    }

    getStatusList = async () => {
        const { dataStore, usersTabStore } = this.rootStore;
        const headers = await this.getInitToken();
        if (headers !== null) {
            fetch(`${Constants.API_URL.CARD_STATUS}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    dataStore.setStatusList(data);
                    usersTabStore.setStatusList(data);
                })
                .catch((e) => {
                    console.log(e)
                    // appStore.setLoading(false);
                });
        }
    }


    getProfile = async () => {
        const { dataStore } = this.rootStore;
        const headers = await this.getInitToken();
        if (headers !== null) {
            fetch(`${Constants.API_URL.PROFILE}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    dataStore.setProfile(data.user);
                })
                .catch((e) => {
                    console.log(e)
                    // appStore.setLoading(false);
                });
        }
    }

    getTitle = async (value) => {
        const { dataStore } = this.rootStore;

        const headers = await this.getInitToken();
        if (headers !== null) {
            fetch(`${Constants.API_URL.GET_TITLE}?url=${value}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    dataStore.setNewCardTitle(data);
                })
                .catch((e) => {
                    //console.log(e)
                    // appStore.setLoading(false);
                });
        }

    }

    getMessages = async (cardId) => {
        const { dataStore } = this.rootStore;
        const headers = await this.getInitToken();
        if (headers !== null) {
            fetch(`${Constants.API_URL.MESSAGES}?filter[card_id]=${cardId}`, headers,)
                .then(status)
                .then(json)
                .then((data) => {
                    dataStore.setMessages(data);
                })
                .catch((e) => {
                    console.log(e)
                    // appStore.setLoading(false);
                });
        }

    }

    createMessage = async (values) => {
        const headers = await this.getInitToken();
        return fetch(Constants.API_URL.MESSAGE_CREATE, {
            method: 'POST',
            ...headers,
            body: JSON.stringify(values)
        })
            .then(status)
            .then(json)
            .then(() => {
                return true;

            })
            .catch((e) => {

                console.log('================ error ==================')
            });
    }


    creatPicker = async (values) => {
        const { pickerStore } = this.rootStore;
        const headers = await this.getInitToken();
        fetch(Constants.API_URL.PICKER_CREATE, {
            method: 'POST',
            ...headers,
            body: JSON.stringify(values)
        })
            .then(status)
            .then(json)
            .then(() => {


                console.log('================ succes ==================')
            })
            .catch((e) => {

                console.log('================ error :', e)
            });
    }

    uploadPhoto = async (values, cardId) => {

        const headers = await this.getInitToken('multipart/form-data;');
        fetch(Constants.API_URL.UPLOAD_PHOTO, {
            method: 'POST',
            ...headers,
            body: values
        })
            .then(status)
            .then(json)
            .then(res => {

                if (res != null) this.sendPhotoToCard(res, cardId);
            })
            .catch((e) => {
                console.log('================ error: ', e);
            });
    }

    uploadPdf = async (value, cardId) => {
        const headers = await this.getInitToken('multipart/form-data;');
        fetch(Constants.API_URL.UPLOAD_AUTOTEKA, {
            method: 'POST',
            ...headers,
            body: value
        })
            .then(status)
            .then(json)
            .then(res => {
                //console.log(res)
                if (res != null) this.sendPdfToCard(res, cardId);
            })
            .catch((e) => {
                console.log('================ error: ', e);
            });
    }

    sendPhotoToCard = async (photos, cardId) => {
        const { dataStore } = this.rootStore;
        let allPhotos = JSON.parse(dataStore.getCardData.photos);
        if (allPhotos) { allPhotos = allPhotos.concat(photos); }
        else { allPhotos = photos }
        let obj = {
            "photos": JSON.stringify(allPhotos)
        };
        const headers = await this.getInitToken();
        fetch(`${Constants.API_URL.CARD}/${cardId}`, {
            method: 'PUT',
            ...headers,
            body: JSON.stringify(obj)
        })
            .then(status)
            .then(json)
            .then(res => {
                dataStore.refreshCard(cardId);
            })
            .catch((e) => {
                console.log('================ error: ', e);
            });
    }

    removePhotoFromCard = async (removePhoto, cardId) => {
        const { dataStore } = this.rootStore;
        let allPhotos = JSON.parse(dataStore.getCardData.photos);
        allPhotos = allPhotos.filter(e => e !== removePhoto);
        let obj = {
            "photos": JSON.stringify(allPhotos)
        };
        const headers = await this.getInitToken();
        fetch(`${Constants.API_URL.CARD}/${cardId}`, {
            method: 'PUT',
            ...headers,
            body: JSON.stringify(obj)
        })
            .then(status)
            .then(json)
            .then(res => {
                dataStore.refreshCard(cardId);
            })
            .catch((e) => {
                console.log('================ error: ', e);
            });
    }

    sendPdfToCard = async (pdf, cardId) => {
        const { dataStore } = this.rootStore;
        let obj = {
            "autoteka": JSON.stringify(pdf)
        };
        const headers = await this.getInitToken();
        fetch(`${Constants.API_URL.CARD}/${cardId}`, {
            method: 'PUT',
            ...headers,
            body: JSON.stringify(obj)
        })
            .then(status)
            .then(json)
            .then(res => {
                dataStore.refreshCard(cardId);
            })
            .catch((e) => {
                console.log('================ error: ', e);
            });
    }



    createCard = async (values) => {
        const { dataStore } = this.rootStore;
        const headers = await this.getInitToken();

        fetch(Constants.API_URL.CARD_CREATE, {
            method: 'POST',
            ...headers,
            body: JSON.stringify(values)
        })
            .then(status)
            .then(json)
            .then((data) => {
                dataStore.setCreateCardStatus('succes', { post: data.title, id: data.id });
            })
            .catch((json) => {
                dataStore.setErrors(JSON.parse(json.message))
                dataStore.setCreateCardStatus('error');
            });
    }

    createUser = async (data) => {
        const { usersTabStore } = this.rootStore;
        const headers = await this.getInitToken();

        let obj = { name: data.name, phone: data.phone, email: data.email, role: usersTabStore.getRoleByName(data.role), password: data.password };

        fetch(Constants.API_URL.USER_CREATE, {
            method: 'POST',
            ...headers,
            body: JSON.stringify(obj)
        })
            .then(status)
            .then(json)
            .then(() => {
                usersTabStore.setCreateUserStatus('succes');
            })
            .catch((e) => {
                usersTabStore.setCreateUserStatus('error');
            });
    }

    getStatistic = async () => {
        const { dashboardsStore } = this.rootStore;
        const { statPage, getActiveFilter } = dashboardsStore;

        const headers = await this.getInitToken();

        if (headers !== null) {
            fetch(`${Constants.API_URL[getActiveFilter]}&page=${statPage}`, headers,)
                .then(status)
                .then((res) => {
                    return { headers: res.headers.map, res: res }
                })
                .then(({ headers, res }) => {
                    dashboardsStore.setStatisticPageCount(headers['x-pagination-page-count']);
                    return res.json()
                })
                .then((data) => {
                    dashboardsStore.setStatistic(data);
                })
                .catch((e) => {
                    console.log(e)
                    // appStore.setLoading(false);
                });
        }
    }

    getUserData = async (userId) => {

        const headers = await this.getInitToken();
        if (headers !== null) {
            return fetch(`${Constants.API_URL.USER}?fields=name,phone&filter[name]=${userId}`, headers,)
                .then(status)
                .then(json)
                .then(data => {
                    return data.users[0];
                })
                .catch((e) => {
                    console.log(e)

                });
        }

    }
}
