import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import DashboardsMainStack from '~/screens/TabDashboards/DashboardsMainStack';
import DashboardListStack from '~/screens/TabDashboards/DashboardListStack';
import DashboardsFilterStack from '~/screens/TabDashboards/DashboardsFilterStack';

const RootStack = createStackNavigator();

export default DashboardsScreenNavigation = () => {
  return (
    <RootStack.Navigator>
      <RootStack.Screen name="DashboardsMainStack" component={DashboardsMainStack} options={{ headerShown: false, title: 'Дашборды' }} />
      
      <RootStack.Screen
                name="DashboardListStack"
                component={DashboardListStack}
                options={({ route }) => {
                    return {
                        headerBackTitleVisible: false,
                        tabBarVisible: false,
                        headerTitleAlign: 'center',
                        title: route.params.post
                    }
                }}
            />
       <RootStack.Screen
                name="DashboardsFilterStack"
                component={DashboardsFilterStack}
                options={({ route }) => {
                    return {
                        headerBackTitleVisible: false,
                        tabBarVisible: false,
                        headerTitleAlign: 'center',
                        title: route.params.post
                    }
                }}
            />      
    </RootStack.Navigator>
  );
};


