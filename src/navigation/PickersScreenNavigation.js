import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import PickersMainStack from '~/screens/TabPickers/PickersMainStack';


const RootStack = createStackNavigator();

export default PickersScreenNavigation = () => {
    return (
        <RootStack.Navigator>
            <RootStack.Screen
                name="PickersMainStack"
                component={PickersMainStack}
                options={{ headerShown: false, title: "Пользователи" }}
            />
        </RootStack.Navigator>
    );
};
