import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import UsersMainStack from '~/screens/TabUsers/UsersMainStack';
//import NewUserStack from '~/screens/TabUsers/NewUserStack';
import UserStack from '~/screens/TabUsers/UserStack';
import { inject } from "mobx-react";

const RootStack = createStackNavigator();

const UsersScreenNavigation = (props) => {

    return (
        <RootStack.Navigator>
            <RootStack.Screen
                name="UsersMainStack"
                component={UsersMainStack}
                options={{ headerShown: false, title: "Пользователи" }}
            />
            {props.appStore.can("Э-СтатистикаПоПользователю") ?
                <RootStack.Screen
                    name="UserStack"
                    component={UserStack}
                    options={({ route }) => {
                        return {
                            headerBackTitleVisible: false,
                            tabBarVisible: false,
                            headerTitleAlign: 'center',
                            title: route.params.post,
                            headerStyle: { backgroundColor: '#fff', elevation: 0 }
                        }
                    }}
                /> : null}
        </RootStack.Navigator>
    );
};

export default inject("appStore")(UsersScreenNavigation);