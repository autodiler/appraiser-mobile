import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import RequestsMainStack from '~/screens/TabRequset/RequestsMainStack';

const RootStack = createStackNavigator();

export default RequestScreenNavigation = () => {
    return (
        <RootStack.Navigator>
            <RootStack.Screen
                name="RequestsMainStack"
                component={RequestsMainStack}
                options={{ headerShown: false, title: "Заявки" }}
            />            
        </RootStack.Navigator>
    );
};
