import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RequestScreenNavigation from '~/navigation/RequestScreenNavigation.js';
import UsersScreenNavigation from '~/navigation/UsersScreenNavigation';
import Notifications from '~/screens/TabNotifications/Notifications';
import DashboardsScreenNavigation from '~/navigation/DashboardsScreenNavigation';

import OptionsStack from '~/screens/TabOptions/OptionsStack';
import { COLORS } from '~/styles';
import { observer, inject } from 'mobx-react';
const Tab = createMaterialTopTabNavigator();


import * as RootNavigation from '~/navigation/RootNavigation';

const TabNavigation = observer((props) => {

  const { notification, setNotification } = props.appStore;



  React.useEffect(() => {
    if (notification) {
      notificationRouteHandler();

    }
  }, [notification]);

  const notificationRouteHandler = () => {

    const { event, params } = notification;

    switch (event) {
      case "new-card":
        RootNavigation.navigate("RequestStack", { id: params.card_id });
        break;
      case "new-card-message":
        RootNavigation.navigate("RequestStack", { id: params.card_id });
        break;
    }

    setNotification(null);
  }

  return (
    <Tab.Navigator
      initialRouteName="Статистика"
      lazy={true}
      tabBarPosition="bottom"
      swipeVelocityImpact={0.5}
      swipeEnabled={true}
      screenOptions={({ route, navigation }) => {
        return { tabBarLabel: navigation.isFocused() ? route.name : '' };
      }}
      tabBarOptions={{
        tabStyle: { paddingLeft: 0, paddingRight: 0 },
        inactiveTintColor: '#8F9BB3',
        activeTintColor: COLORS.mainGreen,
        showIcon: true,
        labelStyle: { fontSize: 8 },
        indicatorStyle: { backgroundColor: COLORS.mainGreen },

      }}
    >
      <Tab.Screen
        name="Статистика"
        component={DashboardsScreenNavigation}
        options={{
          tabBarIcon: ({ color }) => {
            return (
              <Icon name="chart-pie" color={color} size={24} />
            );
          }
        }}
      />
      <Tab.Screen
        name="Заявки"
        component={RequestScreenNavigation}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon name="format-list-bulleted" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name="Уведомления"
        component={Notifications}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon name="bell-ring-outline" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name="Пользователи"
        component={UsersScreenNavigation}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon name="account-supervisor" color={color} size={24} />
          ),
        }}
      />
      <Tab.Screen
        name="Еще"
        component={OptionsStack}
        options={{
          tabBarIcon: ({ color }) => (
            <Icon name="dots-horizontal" color={color} size={24} />
          ),
        }}
      />
    </Tab.Navigator>
  );
});

export default inject('appStore')(TabNavigation);
