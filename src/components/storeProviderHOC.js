import * as React from 'react';
import RootStore from '~/stores/RootStore';

import * as Notifications from 'expo-notifications';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: true,
    }),
});

import * as RootNavigation from '~/navigation/RootNavigation';

const storeProviderHOC = (WrappedComponent, Provider) => {

    const rootStore = new RootStore();

    rootStore.appStore.initAuthToken();

    return class extends React.Component {

        constructor() {
            super();

            this.notificationListener = React.createRef();
            this.responseListener = React.createRef();
        }

        componentDidMount() {
            this.notificationListener.current = Notifications.addNotificationReceivedListener(notification => {

            });

            this.responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
                const { data } = response.notification.request.content;
                rootStore.appStore.setNotification(data);
            });
        }
        componentWillUnmount() {
            Notifications.removeNotificationSubscription(this.notificationListener);
            Notifications.removeNotificationSubscription(this.responseListener);
        }
        render() {
            return (
                <Provider {...rootStore} profile={rootStore.userStore.profile}>
                    <WrappedComponent {...this.props} />
                </Provider>
            );
        }
    };
};

export default storeProviderHOC;