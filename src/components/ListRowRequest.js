import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { COLORS } from '~/styles';
import Status from '~/components/Status';
import * as RootNavigation from '~/navigation/RootNavigation';
import { DeviceConst } from './../constants/constants';
import {
    TouchableRipple
} from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const ListRowRequest = (props) => {

    const { id, title, author_name, created_at, status_id, urgency, expired } = props;

    return (
        <TouchableRipple activeOpacity={0.8} onPress={() => RootNavigation.navigate('RequestStack', { id: id })}>
            <View style={styles.container}>
                <View style={styles.container_text, { paddingBottom: 5 }} >
                    <View style={[styles.titleView, styles.padding]} >
                        <Status status={status_id} />
                        <Text style={styles.containerTitle}
                            numberOfLines={1}>
                            {title}
                        </Text>
                    </View>
                    <Text style={[styles.name, styles.padding]}>
                        {author_name}
                    </Text>
                    <Text style={[styles.date, styles.padding]}>
                        {new Date(created_at).toLocaleDateString() + ' ' + new Date(created_at).toLocaleTimeString().slice(0, -3)}
                    </Text>
                </View>
                {(urgency) ? <Icon name="lightning-bolt-outline" size={20} color={COLORS.mainRed} style={styles.containerUrgently} /> : null}
                {(expired) ? <Text style={styles.containerDelay}>Просрочено</Text> : null}
            </View>
        </TouchableRipple>
    )
};
const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#EDF1F7',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
        alignItems: 'center'

    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',


    },
    containerTitle: {
        fontSize: 20,
        color: '#000212',
        fontWeight: 'bold',
        width: '90%'

    },
    name: {
        fontSize: 18,
        color: '#3F3F5C',
        left: 20,
        marginBottom: 4
    },
    titleView: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    date: {
        fontSize: 16,
        left: 20,
        color: COLORS.default
    },
    containerUrgently: {
        alignItems: "center",
        position: 'absolute',
        right: '5%',
        top: '60%'
    },
    containerDelay: {
        fontSize: 14,
        color: COLORS.mainRed,
        alignItems: "center",
        position: 'absolute',
        right: '5%',
        bottom: '23%'
    },
    padding: {
        marginTop: 5,
        marginBottom: 5,
    }

});

export default ListRowRequest;