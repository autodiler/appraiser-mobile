import React from 'react';
import BarrelCount from './BarrelCount';
import { observer } from 'mobx-react-lite';

const Filters = (props) => {
    const { filters, onPress, hide } = props;
    
    return (
        <>
            {filters.map((filter, index) => {
                return (
                    <BarrelCount
                        idx={index}
                        key={(filter.title+Math.random()).toString()}
                        onPress={() => { onPress(filter.value, index, filter) }}
                        title={filter.title}
                        counter={filter.count}
                        activeBg={filter.activeBg}
                        unactiveBg={filter.unactiveBg}
                        active={true}
                        state={filter.state}
                        active={filter.active}
                        hide={hide} />
                )
            })}
        </>
    )
}

export default observer(Filters);