import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { COLORS } from '~/styles';
import { DeviceConst } from '~/constants/constants';


const FilterCount = (props) => {

  const { count } = props;  
 
    return (
      
        <View style={styles.container}> 
            <Text style={styles.textStyle}>{count}</Text>
        </View>
      
    );
  
}

export default FilterCount;

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
  container: {
    height: 18,
    width: 18,
    borderRadius: 9,
    backgroundColor:COLORS.inworkStatus,
    alignContent:'center',
    alignItems:'center'
  },
  textStyle:{
    color:'white',
    bottom:1
  }
});