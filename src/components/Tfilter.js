import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLORS } from '~/styles';
import { DeviceConst } from '~/constants/constants';
import Picker from '~/components/Picker';
import { inject, observer } from 'mobx-react';
import FilterCount from '~/components/filterCount';


@inject('overallStore', 'dataStore')
@observer
export default class Tfilter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false
    }

  }



  render() {

    return (
      <TouchableOpacity delayPressIn={0.3} style={styles.container} onPress={() => this.setState({ modalVisible: true })}>
        <Text style={styles.textCcontainer}>Фильтр</Text>
        { (this.props.dataStore.filterCount > 0) ? <FilterCount count={this.props.dataStore.getFilterCount} /> : <Icon name="chevron-down" size={20} color={COLORS.border} />}
        <Picker
          multiple={true}
          footerBtn={true}
          footerBtnTitle='Сбросить всё'
          dataList={this.props.dataStore.getFilterCardList}
          onClose={() => this.setState({ modalVisible: false })}
          visible={this.state.modalVisible}
          closable={true}
          onChange={(data) => this.props.dataStore.setFilterCardChecked(data)}
        // onSelectChange={(data) => { console.log(data) }}
        />
      </TouchableOpacity>
    );
  }
}
const margin = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
  container: {
    height: '100%',
    borderRadius: 8,
    marginTop: 5,
    paddingLeft: margin,
    paddingRight: margin,
    backgroundColor: COLORS.defaultFilter,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textCcontainer: {
    marginRight: 10
  }
});