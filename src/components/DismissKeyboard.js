import React from 'react';
import { TouchableWithoutFeedback, Keyboard, View } from 'react-native';

export default DismissKeyboard = ({ children, styles }) => (
  <TouchableWithoutFeedback
    onPress={() => Keyboard.dismiss()}
  >
    <View style={styles}>
      {children}
    </View>
  </TouchableWithoutFeedback>
);
