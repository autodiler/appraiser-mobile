import React from 'react';
import { View, Text, StyleSheet, Linking, TouchableOpacity } from 'react-native';
import { COLORS } from '~/styles';
import { DeviceConst } from '../constants/constants';


import {
    TouchableRipple
} from 'react-native-paper';

const ListRowPicker = (props) => {
    const { comment, name, phone, price, onPressPicker } = props;

    return (
        <TouchableRipple
            delayPressIn={0.3}
            activeOpacity={0.8}
            onPress={onPressPicker ? () => { onPressPicker({ m_comment: comment, m_name: name, m_phone: phone, m_price: price }) } : null}>
            <View style={styles.container}>
                <View style={styles.container_text}>
                    <Text style={[styles.name, styles.padding]}>
                        {name}

                    </Text>

                    <Text style={[styles.price, styles.padding]}>
                        {price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + " ₽"}
                    </Text>
                    <Text style={[styles.phone, styles.padding]}>
                        {phone}
                    </Text>
                    <Text style={[styles.comment, styles.padding]}>
                        {comment}
                    </Text>
                    <TouchableOpacity delayPressIn={0.3} style={styles.callButton} onPress={() => {
                        (DeviceConst.isAndroid) ?
                            Linking.openURL('tel:${' + phone + '}')
                            :
                            Linking.openURL('telprompt:${' + phone + '}')
                    }} >
                    </TouchableOpacity>
                </View>
            </View>
        </TouchableRipple>
    )
};

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#EDF1F7',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
        alignItems: 'center'
    },
    price: {
        fontSize: 18,
        color: 'black',
        fontWeight: 'bold'

    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 12,
        justifyContent: 'center',
    },
    name: {
        fontSize: 16,
        fontStyle: 'normal',
        color: '#000212',
        fontWeight: 'bold'
    },
    phone: {
        fontSize: 18,
        color: '#3F3F5C',
    },
    name_container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    comment: {
        fontSize: 16,
        color: COLORS.default
    },
    padding: {
        marginTop: 5,
        marginBottom: 5,
    },
    callButton: {
        position: 'absolute',
        right: '5%',
        top: '40%',
    },
    logo: {
        width: 20,
        height: 20
    }
});

export default ListRowPicker;