
import * as React from 'react';

const RoleHOC = (WrappedComponent) => {

    return class extends React.Component {

        render() {


            return <WrappedComponent {...this.props} />

        }
    };

};

export default RoleHOC;