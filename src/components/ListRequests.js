import React from 'react';
import { View, FlatList, StyleSheet, RefreshControl, Text } from 'react-native';
import ListRowRequest from '~/components/ListRowRequest';
import { observer } from 'mobx-react';
import LoadIndicator from '~/components/LoadIndicator';



const ListRequests = (props) => {

  const { itemList, loading, pageCount, currentPage, loadMore, refreshing, onRefresh } = props;


  const memoizedValue = React.useMemo(() => { return itemList }, [itemList]);

  const emptyComponent = () => {
    return <LoadIndicator type="empty" />
  }

  return (
    <>
      {
        memoizedValue.length == 0 ? emptyComponent() :
          <>
            <FlatList
              refreshControl={refreshing != undefined ? <RefreshControl refreshing={refreshing} onRefresh={onRefresh} /> : null}
              data={memoizedValue}
              keyExtractor={(item) => (Math.random()).toString()}
              onEndReached={pageCount == currentPage ? null : loadMore}
              onEndReachedThreshold={0.5}
              scrollEventThrottle={160}
              ListFooterComponent={() => {
                if (!loading) return null;
                return (
                  <LoadIndicator />
                );
              }}
              ListFooterComponentStyle={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center', margin: loading ? 20 : 0 }}
              renderItem={({ item }) => {
                return (
                  <ListRowRequest
                    id={item.id}
                    title={item.title}
                    author_name={item.author_name}
                    created_at={item.created_at * 1000}
                    status_id={item.status_id}
                    urgency={item.urgency}
                    expired={item.expired}
                  />
                )
              }}
            />
          </>
      }
    </>
  );
};

export default observer(ListRequests);