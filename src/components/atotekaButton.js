import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

import { COLORS } from '~/styles';
import { DeviceConst } from '~/constants/constants';
import * as DocumentPicker from 'expo-document-picker';
import { inject, observer } from 'mobx-react';
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('fetchStore')
@observer
export default class AutotekaButton extends Component {
  constructor(props) {
    super(props);

  }
  onShare = async (pdfUri, name) => {

    const { uri: localUri } = await FileSystem.downloadAsync(
      pdfUri,
      FileSystem.documentDirectory + 'Autoteka-' + name
    ).catch((error) => {
      console.error(error)
    })
    await Sharing.shareAsync(localUri)
      .catch((err) => console.log('Sharing::error', err))

  }

  _pickDocument = async () => {
    const { fetchStore, id, pdf } = this.props;
    if (pdf != null) {

      let fileUri = 'http://appraiser.mediatver.ru/storage/files/autoteka/';
      fileUri += pdf[0].slice(0, 1);
      fileUri += '/' + pdf[0].slice(0, 2);
      fileUri += '/' + pdf[0].slice(0, 4);
      fileUri += '/' + pdf[0];
      this.onShare(fileUri, pdf[0]);





    } else {
      let result = await DocumentPicker.getDocumentAsync({});
      if (result != undefined) {
        let objPdf = {
          uri: result.uri,
          name: result.name,
          type: 'application/pdf',
        };
        let body = new FormData();
        body.append("files[]", objPdf);
        fetchStore.uploadPdf(body, id);
      }
    }

  }

  render() {
    const { pdf } = this.props;
    return (
      <TouchableOpacity onPress={this._pickDocument} style={styles.container}>
        <Text style={styles.textCcontainer}>
          Автотека
        </Text>
        {(pdf === null) ? <Icon name="plus" size={18} /> : <Icon name="file-download" size={18} color={COLORS.mainGreen}/>}
      </TouchableOpacity>
    )
  }
};




const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
  container: {
    height: '100%',
    marginRight: 8,
    borderRadius: 8,
    paddingLeft: marginLeftRight,
    paddingRight: marginLeftRight,
    backgroundColor: COLORS.defaultFilter,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textCcontainer: {
    marginRight: 10
  }
});