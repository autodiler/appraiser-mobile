import React from 'react';
import {
  StyleSheet, View, Text, TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLORS } from '../styles/index';

export default DashboardsCard = ({ title, color, status, text, onPress, padding }) => {

  const wrapper = (content) => {
    return <View style={styles.statusContent}>{content}<Text style={styles.statusText}>{text}</Text></View>
  }

  const getStatus = () => {
    switch (status) {
      case 'new':
        return wrapper(<Icon name="checkbox-blank-circle" size={10} color={COLORS.mainBlue} />)
      case 'in-work':
        return wrapper(<Icon name="checkbox-blank-circle" size={10} color={COLORS.mainGreen} />)
      case 'buying':
        return wrapper(<Icon name="checkbox-blank-circle" size={10} color={COLORS.buyingStatus} />)
      case 'expired':
        return wrapper(<Icon name="checkbox-blank-circle" size={10} color={COLORS.mainRed} />)
      default:
        return null;
    }
  }

  return (
    <TouchableOpacity delayPressIn={0.3} pressDuration={0} style={[styles.cardContainer, { ...padding }]} onPress={() => { onPress() }}>
      <View style={[styles.cardView, { backgroundColor: color }]}>
        <View style={styles.contentView}>
          <Text style={styles.text}>{title}</Text>
        </View>
        {getStatus()}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cardView: {
    borderRadius: 16,
    height: '70%',
    paddingLeft: 20,
    paddingTop: 15,
    paddingBottom: 20,
  },
  cardContainer: {
    width: '50%',
    paddingTop: 8
  },
  contentView: {
    flex: 0.7
  },
  text: {
    fontSize: 56
  },
  statusContent: {
    height: '50%',
    flex: 0.3,
    flexDirection: 'row',
    alignItems: 'center'
  },
  statusText: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 5
  }
});