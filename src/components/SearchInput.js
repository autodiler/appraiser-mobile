import React from 'react';
import { TextInput, View, StyleSheet, Text } from 'react-native';

const SearchInput = (props) => {

    const { value, defaultValue, maxLength, onChange, textContentType, dataDetectorTypes, keyboardType, secureTextEntry } = props;

    const [focused, setFocused] = React.useState(false)

    const styles = StyleSheet.create({
        
        inputText: {
            height: 60,
            color: '#a9abc3',
            fontSize: 16,
            width: '90%'
        },
        inputText_focused: {
            color: '#00ba88'
        },
        inputView: {
            width: "100%",
            backgroundColor: "white",
            borderRadius: 8,
            height: 48,
            justifyContent: "center",
            alignItems: "center",
            borderColor: '#a9abc3',
            borderWidth: 1,
            borderRadius: 10,
        },
        inputView_focused: {
            borderColor: '#00ba88',
        },
    })

    return (
        <View style={[styles.inputView, focused && styles.inputView_focused]}>
            <TextInput
                maxLength={maxLength}
                value={value}
                onFocus={() => setFocused(true)}
                onBlur={() => setFocused(false)}
                style={[styles.inputText, focused && styles.inputText_focused]}
                placeholder = {"Найти пользователя"}
                defaultValue={defaultValue}
                secureTextEntry={secureTextEntry}
                textContentType={textContentType}
                dataDetectorTypes={dataDetectorTypes}
                keyboardType={keyboardType}
                onChangeText={(text) => {onChange(text)}}
            />
        </View>
    )
}

export default SearchInput;
