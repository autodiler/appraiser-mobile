import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import { COLORS } from '~/styles';
import { DeviceConst } from '~/constants/constants';
import Picker from '~/components/Picker';
import { inject } from 'mobx-react';
import Input from '~/components/Input';


import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const CityPicker = (props) => {
  const { overallStore, dataStore } = props;
  const [modalVisible, setmodalVisible] = React.useState(false);

  const handleSelect = (data) => {
    overallStore.setTownCheckedCard(data);
    dataStore.setFilterCityId(overallStore.getActiveIdTownCard);
    setmodalVisible(false)
  }

  return (

    <TouchableOpacity delayPressIn={0.3} style={styles.container} onPress={() => setmodalVisible(true)}>
      <Text style={styles.textCcontainer}>
        {
          overallStore.getActiveTownCard
        }
      </Text>
      <Icon name="chevron-down" size={20} color={COLORS.border} />
      <Picker
        multiple={true}
        footerBtn={false}
        search={true}
        onClose={() => setmodalVisible(false)}
        visible={modalVisible}
        fullSize={true}
        headerTitle={"Выбор города"}
        SearchNode={Input}
        dataList={overallStore.getCitiesListCard}
        closable={true}
        radioButton={true}
        selectedValue={null}
        onSelectChange={handleSelect}
      />
    </TouchableOpacity>
  );
}



export default inject('overallStore', 'dataStore')(CityPicker);

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
  container: {
    height: '100%',
    marginLeft: marginLeftRight,
    marginTop: 5,
    borderRadius: 8,
    paddingLeft: marginLeftRight,
    paddingRight: marginLeftRight,
    backgroundColor: COLORS.defaultFilter,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textCcontainer: {
    marginRight: 10
  }
});