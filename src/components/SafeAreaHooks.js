
import * as React from 'react';
import { COLORS } from '~/styles'
import { SafeAreaView } from 'react-native-safe-area-context';

const SafeAreaHooks = (WrappedComponent) => {
    return class extends React.Component {

        render() {
            return (
                <SafeAreaView
                    style={{
                        backgroundColor: COLORS.mainBg,
                        flex: 1
                    }}>
                    <WrappedComponent {...this.props} />
                </SafeAreaView>
            );
        }
    };
};

export default SafeAreaHooks;