import React from 'react';
import { TextInput, View, StyleSheet, Text } from 'react-native';
import { COLORS } from '~/styles';


const MInput = (props) => {

  const {
    value,
    defaultValue,
    onChange,
    keyboardType, label,
    placeholder, secureTextEntry,
    margin, height, multiple,   
    autoFocus,    
    AddonAfter,   
    changeFocus
  } = props;

  const [focused, setFocused] = React.useState(false);
  const [secure, setSecure] = React.useState(secureTextEntry);

  const [mHeight,setTextHeight] = React.useState(56);
  

  return (
    <>
      <View style={[styles.inputView, focused && styles.inputView_focused, { ...margin }]}>
        {label ? <Text style={[styles.placeholder, focused && styles.placeholder_focused]}>{label}</Text> : null}
        <TextInput
          multiline={true}
          value={value}
          autoFocus={autoFocus}
          onFocus={() => { changeFocus ? null : setFocused(true) }}
          onBlur={() => { changeFocus ? null : setFocused(false) }}
          style={[styles.inputText, focused && styles.inputText_focused, { height: mHeight, minHeight: height }, AddonAfter && styles.inputText_focused_addon]}
          onContentSizeChange={(event) => setTextHeight(event.nativeEvent.contentSize.height)}
          defaultValue={defaultValue}
          secureTextEntry={secure}
          textAlignVertical="center"
          editable={multiple ? false : true}
          keyboardType={keyboardType}
          placeholderTextColor="#bec6d6"
          onChangeText={(text, e) => { onChange(text, e); }}
          placeholder={placeholder}
        />
      </View>
      
    </>
  );
};

const styles = StyleSheet.create({
  placeholder: {
    color: COLORS.default,
    fontSize: 14,
    position: 'absolute',
    top: -11,
    left: 15,
    backgroundColor: COLORS.mainBg,
    paddingLeft: 5,
    paddingRight: 5
  },
  placeholder_focused: {
    color: COLORS.mainGreen
  },
  errorText: {
    color: 'red'
  },
  inputText: {
    padding: 12,
    //paddingBottom: 5,
    height: 48,
    color: COLORS.default,
    fontSize: 18,
    width: '100%'
  },
  inputText_focused_addon: {
    width: '85%'
  },
  inputText_focused: {
    color: COLORS.mainGreen
  },
  inputView: {
    width: 'auto',
    flexDirection: 'row',
    backgroundColor: COLORS.mainBg,
    borderRadius: 8,
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderColor: COLORS.border,
    borderWidth: 1,
  },
  inputView_focused: {
    borderColor: COLORS.mainGreen,
  },
  icon: {
    position: 'absolute',
    right: 8
  }
});

export default MInput;