import React from 'react';
import { StyleSheet } from 'react-native';
import { Button as Btn } from 'react-native-elements';
import { inject } from 'mobx-react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const AddButton = (props) => {

    const rule = "С-Пользователи-Добавление";

    const { label, onPress, icon } = props;

    if (!props.appStore.can(rule)) {
        return null;
    }

    return (
        <Btn
            icon={icon}
            onPress={() => { onPress() }}
            title={label}
            titleStyle={styles.btnText}
            buttonStyle={styles.btnTextContainer}
            containerStyle={styles.btnContainer}
        />
    )
}

const styles = StyleSheet.create({
    btnContainer: {
        width: 56,
        backgroundColor: "#00ba88",
        borderRadius: 16,
        height: 56,
        position: "absolute",
        right: '5%',
        bottom: '5%'
    },
    btnTextContainer: {
        backgroundColor: "#00ba88",
        width: '100%',
        height: '100%'
    },
    btnText: {
        color: "white",
        fontSize: 24,
    }
})

export default inject('appStore')(AddButton);
