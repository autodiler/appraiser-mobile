import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { COLORS } from '~/styles';
import { DeviceConst } from '~/constants/constants';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const CityButton = (props) => {
    const { cityName } = props;


    return (

        <View style={styles.container} onPress={() => setmodalVisible(true)}>
            <Text style={styles.textCcontainer}>
                {
                    cityName
                }
            </Text>
            <Icon name="map-marker" size={20} color={COLORS.mainGreen} />

        </View>
    );
}



export default CityButton;

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
    container: {
        height: '100%',
        marginRight: 8,
        borderRadius: 8,
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
        backgroundColor: COLORS.defaultFilter,
        flexDirection: 'row',
        alignItems: 'center'
    },
    textCcontainer: {
        marginRight: 10
    }
});