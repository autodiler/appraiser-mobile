import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLORS } from '../styles/index';

const CallButton = (props) => {

    const { onPress } = props;

    const styles = StyleSheet.create({
        container: {
            alignItems: "center",
            position: 'absolute',
            right: '5%',
        },
        logo: {
            width: 20,
            height: 20
        }
    })

    return (
        <TouchableOpacity delayPressIn={0.3} onPress={() => { onPress() }} style={styles.container}>
            <Icon name="phone" size={20} color={COLORS.border}/>
        </TouchableOpacity>
    )
}




export default CallButton;