import React from 'react';
import { TouchableOpacity, StyleSheet, Text, Modal, View, TouchableHighlight } from 'react-native';
import { COLORS } from '~/styles';
import { Calendar } from 'react-native-calendars';
import { LocaleConfig } from 'react-native-calendars';
import moment, { localeData } from 'moment';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

LocaleConfig.locales['fr'] = {
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],

};

LocaleConfig.defaultLocale = 'fr';

const PeriodButton = (props) => {
    const { onAply } = props;
    const [modalVisible, setModalVisible] = React.useState(false);


    const [state, setState] = React.useState({
        markedDates: {},
        isStartDatePicked: false,
        isEndDatePicked: false,
        startDate: ''
    });

    const onDayPress = (day) => {
        if (state.isStartDatePicked == false) {
            let markedDates = {}
            markedDates[day.dateString] = { startingDay: true, color: COLORS.mainGreen, textColor: '#FFFFFF' };

            setState({
                markedDates: markedDates,
                isStartDatePicked: true,
                isEndDatePicked: false,
                startDate: day.dateString,
            });
        } else {
            let markedDates = state.markedDates
            let startDate = moment(state.startDate);
            let endDate = moment(day.dateString);
            let range = endDate.diff(startDate, 'days')
            if (range > 0) {
                for (let i = 1; i <= range; i++) {
                    let tempDate = startDate.add(1, 'day');
                    tempDate = moment(tempDate).format('YYYY-MM-DD')
                    if (i < range) {
                        markedDates[tempDate] = { color: COLORS.lightGreen, textColor: 'black' };
                    } else {
                        markedDates[tempDate] = { selected: true, endingDay: true, color: COLORS.mainGreen, textColor: '#FFFFFF' };
                    }
                }

                setState({
                    markedDates: markedDates,
                    isStartDatePicked: false,
                    isEndDatePicked: true,
                    startDate: ''
                })

            } else {
                alert('Выберите дату окончания периода');
            }
        }
    };

    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}>
                <View style={styles.mainContainer}>
                    <View style={styles.modalView}>
                        <Calendar
                            style={styles.calendar}
                            current={localeData}
                            minDate={state.startDate}
                            maxDate={'2030-12-31'}
                            disableAllTouchEventsForDisabledDays
                            firstDay={1}
                            markingType={'period'}
                            theme={{ textMonthFontWeight: 'bold', todayTextColor: '#00adf5', }}
                            renderArrow={(direction) => direction == 'right' ? (<Icon name="chevron-right" size={20} color={COLORS.mainGreen} />) : (<Icon name="chevron-left" size={20} color={COLORS.mainGreen} />)}
                            onDayPress={onDayPress}
                            markedDates={JSON.parse(JSON.stringify(state.markedDates))}
                        />
                        <View style={styles.buttonContainer}>
                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: COLORS.mainGreen }}
                                onPress={() => {
                                    setModalVisible(!modalVisible);
                                    onAply(Object.keys(state.markedDates));
                                }}
                            >
                                <Text style={{ ...styles.textStyle, color: COLORS.mainBg }}>Показать</Text>
                            </TouchableHighlight>
                            <TouchableHighlight
                                style={{ ...styles.openButton, backgroundColor: COLORS.defaultFilter }}
                                onPress={() => {
                                    setState({ markedDates: {}, isStartDatePicked: false, isEndDatePicked: false, startDate: '' })
                                }}>
                                <Text style={{ ...styles.textStyle, color: COLORS.mainRed }}>Сбросить</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>
            </Modal>
            <TouchableOpacity delayPressIn={0.3} onPress={() => { setModalVisible(true); }} style={styles.container}>
                <Text style={styles.textContainer}>Период </Text>
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        // =width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, .5)'
    },
    container: {
        height: 32,
        borderRadius: 8,
        alignItems: 'center',
        backgroundColor: COLORS.defaultFilter,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        paddingRight: 8,
        marginTop: 8,
        marginBottom: 8
    },
    textContainer: {
        paddingLeft: 12,
        fontSize: 14
    },
    buttonContainer: {
        flexDirection: 'row',
    },
    modalView: {
        top: 104,
        margin: 20,
        backgroundColor: COLORS.mainBg,
        borderRadius: 20,
        padding: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 16,
        alignContent: 'flex-end',
        alignItems: 'center',
        height: '100%',
        width: '45%',
    },
    textStyle: {
        padding: 12,
        fontSize: 16,
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {

        textAlign: "center"
    },

})

export default PeriodButton;