import React from 'react';
import { View, FlatList, StyleSheet, RefreshControl } from 'react-native';
import ListRowUser from './ListRowUser';
import { observer } from 'mobx-react';
import LoadIndicator from '~/components/LoadIndicator'

const ListUsers = (props) => {

    const { itemList, pageCount, currentPage, loading, onPressUser, loadMore, refreshing, onRefresh, topTextLabel, middleTextLabel, bottomTextLabel, addonAfter } = props;  
    const memoizedValue = React.useMemo(() => { return itemList }, [itemList]);
    const emptyComponent = () => {
        return (
            <LoadIndicator />
        )
    }

    return (
        <>
            {
                memoizedValue.length == 0 ? emptyComponent() :
                    <FlatList
                        refreshControl={refreshing != undefined ? <RefreshControl refreshing={refreshing} onRefresh={onRefresh} /> : null}
                        data={memoizedValue}
                        keyExtractor={(item, index) => `${topTextLabel + index}`}
                        onEndReached={pageCount == currentPage ? null : loadMore}
                        onEndReachedThreshold={0.5}
                        scrollEventThrottle={160}
                        ListFooterComponent={() => {
                            if (!loading) return null;
                            return (
                                <LoadIndicator />
                            );
                        }}
                        ListFooterComponentStyle={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center', margin: loading ? 20 : 0 }}
                        renderItem={({ item }) =>
                            <ListRowUser
                                onPressUser={onPressUser}
                                addonAfter={addonAfter}
                                topTextLabel={item[topTextLabel]}
                                middleTextLabel={item[middleTextLabel]}
                                bottomTextLabel={item[bottomTextLabel]}
                                phone={item.phone}
                                status={item.status}
                                id={item.userId} />
                        }
                    />
            }
        </>
    );
};


export default observer(ListUsers);