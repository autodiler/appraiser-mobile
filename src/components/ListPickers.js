import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import ListRowPicker from './ListRowPicker';
import { observer } from 'mobx-react';

const ListPickers = (props) => {

    const { itemList, onPressPicker } = props;

    return (
        <View style={styles.container}>
            <FlatList
                data={itemList}
                keyExtractor={(item,index) => `${item + index}`}
                renderItem={({ item }) =>
                    <ListRowPicker 
                                    onPressPicker={onPressPicker}    
                                    comment={item.comment}
                                    name={item.name}
                                    phone={item.phone}
                                    price={item.price}

                    />
                }
            />

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flex: 1,
    },
});

export default observer(ListPickers);