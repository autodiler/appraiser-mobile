import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { COLORS } from '~/styles';
import { DeviceConst } from '~/constants/constants';
import { inject, observer } from 'mobx-react';
import Picker from '~/components/Picker';
import Modal from 'react-native-modal';
import Button from '~/components/Button';
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
@inject('dataStore', 'fetchStore')
@observer
export default class StatusButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmModal: false,
      newSelectItems: null,
      modalVisible: false,
    };

  }

  onSelectedNewChange = (newStatus) => {
    this.setState(() => ({
      newSelectItems: newStatus,
      modalVisible: false,
    }))
  }

  getColorByStatus = (status) => {
    switch (status) {
      case 'Новая':
        return COLORS.newStatus;
      case 'В работе':
        return COLORS.inworkStatus;
      case 'В ожидании':
        return COLORS.expiredStatus;
      case 'Выкуплен':
        return COLORS.buyingStatus;
      case 'Отказ':
        return COLORS.rejectionStatus;
    }
  }

  changeStatus = (data) => {
    const { card_id, fetchStore, dataStore } = this.props;
    if (data.item.key == 'purchased') {
      this.setState({ modalVisible: false });
      this.handleConfirm();
    } else {
      fetchStore.changeCard(card_id, { status_id: data.item.key });
      fetchStore.changeCard(card_id, { executor_id: dataStore.getOwnId });
      this.setState({ modalVisible: false });
    }
    fetchStore.getCardsListData('refresh');
  }

  handleConfirm = () => {
    this.setState({
      confirmModal: !this.state.confirmModal
    })
  }

  purchasedCard = (buy_method) => {
    const { fetchStore, card_id } = this.props;
    this.handleConfirm();
    fetchStore.changeCard(card_id, { status_id: 'purchased', buy_method: buy_method })
  }

  render() {
    const { status_id, status } = this.props;

    return (
      <TouchableOpacity delayPressIn={0.3} style={[styles.container, { backgroundColor: this.getColorByStatus(status) }]}
        onPress={status == 'Выкуплен' ? null : () => this.setState({ modalVisible: true })}>
        <Text style={styles.textCcontainer}>{status}</Text>
        {status == 'Выкуплен' ? null : <Icon name="chevron-down" color={COLORS.mainBg} size={20}/>}
        <Picker
          multiple={false}
          footerBtn={true}
          radioButton={true}
          footerBtnTitle='Удалить заявку'
          selectedValue={status_id}
          dataList={this.props.dataStore.getCardStatusList}
          onClose={() => { this.setState({ modalVisible: false }) }}
          visible={this.state.modalVisible}
          closable={true}
          onSelectChange={(data) => { this.changeStatus(data) }}
        />
        <Modal
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={this.state.confirmModal}
          style={styles.confirm_modal}
          onBackdropPress={() => { this.setState({ confirmModal: false }) }}
          onStartShouldSetResponder={() => { return true }}>
          <View style={styles.confirm} >
            <Text style={styles.confirm_text}>Как выкуплен автомобиль?</Text>
            <View style={styles.confirm_button}>
              <View style={styles.confirm_button_item}>
                <Button label="Частник" onPress={() => this.purchasedCard(0)} />
              </View>
              <View style={styles.confirm_button_item}>
                <Button label="Автосалон" onPress={() => this.purchasedCard(1)} />
              </View>
            </View>
          </View>
        </Modal>
      </TouchableOpacity>
    );
  }
}

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(3);
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    height: '100%',
    borderRadius: 8,
    paddingLeft: marginLeftRight,
    paddingRight: marginLeftRight,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textCcontainer: {
    marginRight: 10,
    color: COLORS.mainBg
  },
  confirm_modal: {
    padding: 0,
    margin: 10,
    width: deviceWidth - 20,
  },
  confirm: {
    borderRadius: 18,
    backgroundColor: 'white',
    padding: 20,
    alignItems: 'center'
  },
  confirm_text: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  confirm_button: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-around'
  },
  confirm_button_item: {
    width: '48%',
  }
});