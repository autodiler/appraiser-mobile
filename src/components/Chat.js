import React, { Component } from 'react';
import { GiftedChat, Send, Bubble, MessageText } from 'react-native-gifted-chat';
import { StyleSheet, View, TouchableOpacity, Keyboard, Linking } from 'react-native';
import { inject, observer } from 'mobx-react';
import Input from '~/components/Input';
import { DeviceConst } from './../constants/constants';
import { COLORS } from '~/styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('fetchStore', 'dataStore', 'usersTabStore')
@observer
export default class Chat extends Component {

  constructor(props) {
    super(props);
    this.store = [];
    this.intervalId = setInterval(() => props.fetchStore.getMessages(props.cardId), 5000);
    this.ownId = props.dataStore.getOwnId;
    this.ownName = props.dataStore.getOwnName;
    this.state = {
      message: '',
      height: 0
    }

  }

  renderMessageText = (props) => (
    <MessageText
      {...props}
      //       textStyle={{
      //   left: { color: props.currentMessage.statusColor },
      //   right: { color: props.currentMessage.statusColor },
      // }}
      wrapperStyle={{
        left: { color: props.currentMessage.statusColor },
        right: { color: props.currentMessage.statusColor },
      }}

    />
  );

  renderBubble = (item) => (
    <Bubble
      {...item}
      usernameStyle={{
        fontSize: 14,
        color: '#A9ABC3'
      }}
      textStyle={{
        left: {
          color: 'black',
        },
        right: {
          color: 'black',
        },
      }}
      wrapperStyle={{
        left: {
          backgroundColor: item.currentMessage.statusColor,
        },
        right: {
          backgroundColor: item.currentMessage.statusColor,
        },
      }}
    />
  );

  componentDidMount() {
    const { fetchStore, cardId, navigation } = this.props;

    fetchStore.getMessages(cardId);

    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);

  }

  _keyboardDidShow = (e) => {
    this.setState({ height: e.endCoordinates.height });
  }

  _keyboardDidHide = () => {
    this.setState({ height: 0 });
  }

  componentWillUnmount() {
    this.store = [];
    Keyboard.removeListener('keyboardDidShow', this._keyboardDidShow);
    Keyboard.removeListener('keyboardDidHide', this._keyboardDidHide);
    clearInterval(this.intervalId);
  }

  onSend = async () => {
    const { fetchStore, cardId } = this.props;
    let newMessage = {
      "card_id": cardId,
      "message": this.state.message
    }

    this.setState({ message: '' })
    await fetchStore.createMessage(newMessage).then(res => {
      fetchStore.getMessages(cardId);
    });

  }

  onCall = async () => {
    const { author, executor, fetchStore } = this.props;

    let phoneNumber = null;
    if (this.ownName == author) {
      phoneNumber = fetchStore.getUserData(executor)
    }
    else if (this.ownName == executor) {
      phoneNumber = fetchStore.getUserData(author);
    }
    else {
      phoneNumber = fetchStore.getUserData(author);
    }
    await phoneNumber.then(data => {
      if (data.phone != undefined) {
        (DeviceConst.isAndroid) ?
          Linking.openURL(`tel:${data.phone}`)
          :
          Linking.openURL(`telprompt:${data.phone}`)
      }
    })
  }

  renderSend = (props) => {
    return (
      <Send {...props} containerStyle={styles.sendContainer}>
        <Icon name="send" color={COLORS.default} size={24} />
      </Send>
    );
  }

  render() {
    const { dataStore } = this.props;
    this.store = dataStore.getMessageList.slice().reverse();

    return (

      <GiftedChat
        messages={this.store}
        placeholder='Сообщение'
        messagesContainerStyle={{ backgroundColor: '#edeff3' }}
        alwaysShowSend={true}
        renderUsernameOnMessage={true}
        timeFormat={'HH:mm'}
        renderAvatar={null}
        forceGetKeyboardHeight={true}
        isKeyboardInternallyHandled={false}
        minInputToolbarHeight={64}
        renderMessageText={this.renderMessageText}
        parsePatterns={() => [
          {
            pattern: /#(\w+)/,
            style: { color: COLORS.mainGreen },
            onPress: props => {
              (DeviceConst.isAndroid) ?
                Linking.openURL(`tel:${props.replace('#', '')}`)
                :
                Linking.openURL(`telprompt:${props.replace('#', '')}`)
            },
          },
        ]}
        renderInputToolbar={(props) => {
          return (
            <View style={styles.inputView}>
              <TouchableOpacity delayPressIn={0.3} style={styles.call_button}>
                <Icon name="dialpad" size={24} color={COLORS.default} onPress={() => this.onCall()} />
              </TouchableOpacity>
              <View style={{ flex: 1 }}>
                <Input
                  changeFocus={true}
                  value={this.state.message}
                  onChange={(val) => { this.setState({ message: val }) }}
                  AddonAfter={() => {
                    return (
                      <Icon name="send" size={24} color={COLORS.default} />
                    )
                  }}
                  placeholder="Сообщение"
                  onAddonAfterPress={() => { this.onSend() }} />
              </View>
            </View>
          )
        }
        }
        timeTextStyle={{
          left: { color: '#A9ABC3' },
          right: { color: '#A9ABC3' }
        }}
        renderBubble={this.renderBubble}
        renderSend={this.renderSend}
        user={{
          _id: this.ownId, name: this.ownName
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  sendContainer: {
    // justifyContent: 'center',
    // alignItems: 'flex-start',
    // alignSelf: 'center',
    // marginRight: 15,
  },
  right: {
    color: 'green'
  },
  inner: {
    flex: 1,
    justifyContent: "space-around"
  },
  inputView: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'auto',
    alignItems: 'center',
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 16,
    paddingRight: 16,
    backgroundColor: '#FFFFFF',
  },
  call_button: {
    marginRight: 16,
  }
});

