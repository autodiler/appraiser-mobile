
import React from 'react';
import { Dimensions, Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { CheckBox } from 'react-native-elements';
import { COLORS } from '~/styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { observer } from 'mobx-react-lite';

const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

const Picker = observer((props) => {

    const { dataList, search, visible, fullSize, closable, onSelectChange, footerBtn, footerBtnTitle, onClose, onChange, SearchNode, radioButton, selectedValue } = props;
    const [searchStr, setSearchStr] = React.useState('');

    const handleSearch = (text) => {
        setSearchStr(text)
    }

    const changeHandler = (index) => {

        dataList[index].checked = !dataList[index].checked;
        let selectKeys = [];

        dataList.forEach(element => {
            if (element.checked == true) {
                selectKeys.push(element.title);
            }
        });
        onChange(selectKeys);
    }

    const clearAll = () => {
        dataList.forEach(element => {
            element.checked = false;
        });
        onChange([]);
    }

    const size = {
        height: 56,
        margin: {
            marginTop: 16
        }
    }

    const renderHeader = () => {
        return (
            <>
                {
                    search ? <View
                        style={{
                            backgroundColor: '#fff',
                            padding: 10,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }
                        }>
                        <SearchNode
                            value={searchStr}
                            onChange={handleSearch}
                            label="Выбор города"
                            {...size} />
                    </View > : null}
            </>
        )
    }


    return (
        <Modal
            onBackdropPress={onClose}
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={visible}
            onStartShouldSetResponder={() => { return true }}
            style={[styles.modalContainer, fullSize && styles.modalContainer_full_size]}>
            <View renderToHardwareTextureAndroid={true} style={styles.container}>
                {closable ?
                    <TouchableOpacity delayPressIn={0.3} style={styles.icon} onPress={onClose}>
                        <Icon name="close" size={20} />
                    </TouchableOpacity>
                    : null}
                <View onStartShouldSetResponder={() => { return false }} style={[styles.flatList, !footerBtn && styles.flatList_full_size]}>
                    {renderHeader()}
                    <FlatList
                        keyboardShouldPersistTaps="handled"
                        keyExtractor={(item, index) => index.toString()}
                        data={dataList.filter((item) => {
                            return item.title.toLowerCase().includes(searchStr.toLocaleLowerCase());
                        })}
                        renderItem={({ item, index }) => {
                            return (
                                <>
                                    {!radioButton ? <View onStartShouldSetResponder={() => { return true }} style={styles.itemList}>
                                        <CheckBox
                                            checked={item.checked}
                                            title={item.title}
                                            uncheckedIcon={<Icon name="checkbox-blank-outline" size={26} color={COLORS.border} />}
                                            checkedIcon={<Icon name="check-box-outline" size={26} color={COLORS.mainGreen} />}
                                            containerStyle={styles.checkbox}
                                            titleProps={{
                                                style: styles.item_checkbox
                                            }}
                                            onPress={() => { changeHandler(index) }}
                                            tintColors={{ true: COLORS.mainGreen, false: COLORS.default }} />
                                    </View> :
                                        <TouchableOpacity delayPressIn={0.3} onStartShouldSetResponder={() => { return true }} onPress={() => props.onSelectChange({ item, index })} style={styles.itemList} >
                                            <>
                                                <Text style={styles.item}>{item.title}</Text>
                                                {(fullSize ? item.checked : item.key == selectedValue) ? <Icon name="check" size={24} color={COLORS.mainGreen} style={{ paddingTop: 10 }} /> : null}
                                            </>
                                        </TouchableOpacity>}
                                </>
                            )
                        }}
                    />
                </View>
                {footerBtn ?
                    <TouchableOpacity style={styles.flatListFooter} onPress={() => { clearAll() }}>
                        <Text style={styles.textFooter}>{footerBtnTitle}</Text>
                    </TouchableOpacity> : null}
            </View>
        </Modal>
    );
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    flatList: {
        flex: 0.9,
        alignContent: 'center'
    },
    flatList_full_size: {
        flex: 1
    },
    flatListFooter: {
        justifyContent: 'center',
        flex: 0.1,
        padding: 10,
    },
    textFooter: {
        fontSize: 16,
        color: '#FF3D71',
        fontWeight: '400'
    },
    itemList: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    checkbox: {
        padding: 10,
        marginTop: 10,
        backgroundColor: '#ffffff',
        borderWidth: 0,
        margin: 0,
        flex: 1,
        alignContent: 'center'
    },
    checked: {
        marginTop: 10
    },
    item: {
        padding: 10,
        marginTop: 10,
        fontSize: 16,
        fontWeight: '400',
    },
    item_checkbox: {
        fontSize: 16,
        padding: 0,
        marginTop: 0,
        marginLeft: 14
    },
    icon: {
        position: 'absolute',
        right: 10,
        zIndex: 100
    },
    modalContainer: {
        width: deviceWidth - 20,
        marginTop: 30,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#ffffff',
        borderRadius: 16,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 26,
        paddingBottom: 26
    },
    modalContainer_full_size: {
        flex: 1,
        bottom: null,
        position: 'relative',
        padding: 10

    }
});

export default Picker;