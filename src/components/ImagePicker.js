import React from 'react';
import { Image, StyleSheet, ScrollView, TouchableOpacity, View, Alert, Linking } from 'react-native';
import PhotoButton from './../../svg/add_photo.svg';
import * as RootNavigation from '~/navigation/RootNavigation';
//import ImageView from "react-native-image-viewing";


export default function ImgPicker(props) {
  const { id, photos } = props;
  const [visible, setIsVisible] = React.useState(false);
  const [index, setIndex] = React.useState(0);

  const handlerLongClick = () => {
    return Alert.alert(
      "Внимание!",
      "Вы точно хотите удалить данное фото?",
      [
        { text: "Да" },
        {
          text: 'Отмена',
          //onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
      ],
      { cancelable: false }
    );
  };

  const renderPhotoPreview = (photo) => {
    let uri = 'http://appraiser.mediatver.ru/storage/images/card/78x44/';
    uri += photo.slice(0, 1);
    uri += '/' + photo.slice(0, 2);
    uri += '/' + photo.slice(0, 4);
    uri += '/' + photo;
    return uri;
  }

  const renderPhoto = () => {
    let imageArr = [];
    for (let i = 0; i < photos.length; i++) {
      let ur = 'http://appraiser.mediatver.ru/storage/images/card/source/';
      ur += photos[i].slice(0, 1);
      ur += '/' + photos[i].slice(0, 2);
      ur += '/' + photos[i].slice(0, 4);
      ur += '/' + photos[i];
      imageArr.push({ uri: ur })

    }

    return imageArr;
  }




  return (
    <>
      <View style={styles.container}>
        <ScrollView horizontal={true} style={styles.containerPhoto} showsHorizontalScrollIndicator={false}>
          <TouchableOpacity onPress={() => { RootNavigation.navigate('ChoosePhotoStack', { id: id }) }} style={styles.button}>
            <PhotoButton />
          </TouchableOpacity>
          <View style={styles.containerPhoto}>
            {photos.map((item, index) => (
              <TouchableOpacity key={Math.random()} onPress={() => { setIsVisible(true); setIndex(index) }} onLongPress={handlerLongClick}>
                <Image source={{ uri: renderPhotoPreview(item) }} style={styles.thumbnail} />
              </TouchableOpacity>))}
          </View>
        </ScrollView>
      </View>
      {/* <ImageView
        images={renderPhoto()}
        imageIndex={index}
        visible={visible}
        onRequestClose={() => setIsVisible(false)}
        doubleTapToZoomEnabled={true}

      /> */}
    </>

  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  containerPhoto: {
    flexDirection: 'row',

  },

  button: {
    paddingRight: 4,
    borderRadius: 5,
  },

  thumbnail: {
    width: 78,
    height: 44,
    //resizeMode: 'contain',
    borderRadius: 8,
    margin: 4
  }
});
