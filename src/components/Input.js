import React from 'react';
import {
  TextInput, View, StyleSheet, Text, TouchableOpacity
} from 'react-native';
import { COLORS } from '~/styles';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Input = (props) => {

  const {
    value,
    defaultValue,
    maxLength,
    onChange,
    textContentType,
    dataDetectorTypes,
    keyboardType, label,
    placeholder, secureTextEntry,
    margin, height, multiple,
    onIconPress,
    autoFocus,
    AddonAfter,
    onAddonAfterPress,
    changeFocus,
    hasError,
    resetError
  } = props;

  const [focused, setFocused] = React.useState(false);
  const [secure, setSecure] = React.useState(secureTextEntry);

  const styles = StyleSheet.create({
    placeholder: {
      color: hasError ? COLORS.mainRed : COLORS.default,
      fontSize: 14,
      position: 'absolute',
      top: -11,
      left: 15,
      backgroundColor: COLORS.mainBg,
      paddingLeft: 5,
      paddingRight: 5
    },
    placeholder_focused: {
      color: COLORS.mainGreen
    },
    inputText: {
      padding: 12,
      //paddingBottom: 5,
      height: 48,
      color: COLORS.default,
      fontSize: 18,
      width: '100%'
    },
    inputText_focused_addon: {
      width: '85%'
    },
    inputText_focused: {
      color: COLORS.mainGreen
    },
    inputView: {
      width: 'auto',
      flexDirection: 'row',
      backgroundColor: COLORS.mainBg,
      borderRadius: 8,
      justifyContent: 'flex-start',
      alignItems: 'center',
      borderColor: hasError ? COLORS.mainRed : COLORS.border,
      borderWidth: 1,
    },
    inputView_focused: {
      borderColor: COLORS.mainGreen,
    },
    icon: {
      position: 'absolute',
      right: 8
    }
  });

  return (
    <>
      <View style={[styles.inputView, focused && styles.inputView_focused, { ...margin }]}>
        {label ? <Text style={[styles.placeholder, focused && styles.placeholder_focused]}>{label}</Text> : null}
        <TextInput
          //multiline={textContentType == "password" ? false : true}
          maxLength={maxLength}
          value={value}
          autoFocus={autoFocus}
          onFocus={() => { changeFocus ? null : setFocused(true) }}
          onBlur={() => { changeFocus ? null : setFocused(false) }}
          style={[styles.inputText, focused && styles.inputText_focused, { height: height ? height : 48 }, AddonAfter && styles.inputText_focused_addon]}
          defaultValue={defaultValue}
          secureTextEntry={secure}
          textAlignVertical="center"
          textContentType={textContentType}
          editable={multiple ? false : true}
          dataDetectorTypes={dataDetectorTypes}
          keyboardType={keyboardType}
          placeholderTextColor="#bec6d6"
          onChangeText={(text, e) => { if (hasError) { resetError(false) } onChange(text, e); }}
          placeholder={placeholder}
        />
        {secureTextEntry ? (
          <TouchableOpacity style={styles.icon} onPress={() => setSecure(!secure)}>
            {secure ? <Icon name="eye-off-outline" size={26} color="#a9abc3" /> : <Icon name="eye-outline" size={26} color="#a9abc3" />}
          </TouchableOpacity>
        ) : null}
        {multiple ?
          <>
            <TouchableOpacity style={styles.icon} onPress={onIconPress}>
              <Icon name="chevron-down" color="#a9abc3" size={30} />
            </TouchableOpacity>
          </>
          : null}
        {AddonAfter ?
          <TouchableOpacity delayPressIn={0.3} style={styles.icon} onPress={onAddonAfterPress}>
            {AddonAfter()}
          </TouchableOpacity>
          : null}
      </View>
    </>
  );
};



export default Input;