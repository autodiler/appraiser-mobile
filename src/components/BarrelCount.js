import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { DeviceConst } from './../constants/constants';
const margin = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(2);

BarrelCount = (props) => {

    const { title, activeBg, unactiveBg, counter, state, onPress, active, hide } = props;

    const colorBg = active ? activeBg : unactiveBg;
    const colorStatus = active ? unactiveBg : activeBg;
    const colorTxt = active ? 'white' : 'black';

    const styles = StyleSheet.create({
        container: {
            flex: 1,
            marginLeft: 8,
            marginBottom: margin,
            marginTop: margin
        },
        button: {
            justifyContent: 'center',
            borderRadius: 16,
            backgroundColor: colorBg,
            padding: 10,
            flexDirection: 'row',
            height: 48,
        },
        titleText: {
            color: colorTxt,
            fontSize: 16,
            fontWeight: 'bold'
        },
        containerContent: {
            flexDirection: 'row',
            alignItems: 'center',
        },
        counter: {
            color: colorTxt,
            fontSize: 12,
            fontWeight: 'bold',
            paddingLeft: 5
        },
        StatusBar: {
            marginRight: 8,
            width: 8,
            height: 8,
            backgroundColor: colorStatus,
            borderRadius: 4
        }
    });

    return (
        <View style={styles.container}>
            <TouchableOpacity
                delayPressIn={0.3}
                touchSoundDisabled={false}
                style={styles.button}
                onPress={onPress}>
                <>
                    <View style={styles.containerContent}>
                        {/* <Status status={state} style={{fill:'white'}}/> */}
                        {(hide || state == 'all') ? null : <View style={styles.StatusBar}></View>}
                        <Text style={styles.titleText}>{title}</Text>
                    </View>
                    {counter ? <Text style={styles.counter}>{counter}</Text> : null}
                </>
            </TouchableOpacity>
        </View>
    );
};



export default BarrelCount;
