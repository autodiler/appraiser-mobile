import React from 'react';
import { View, StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { COLORS } from '../styles/index';

const Status = (props) => {

    const { status, style } = props;

    const checkSwitch = (param) => {

        switch (param) {
            case 'new':
                return (<Icon color={COLORS.newStatus} name="checkbox-blank-circle" size={10} />)
            case 'online':
                return (<Icon color={COLORS.inworkStatus} name="checkbox-blank-circle" size={10} />)
            case 'inwork':
                return (<Icon color={COLORS.mainGreen} name="checkbox-blank-circle" size={10} />)
            case 'offline':
                return (<Icon color={COLORS.border} name="checkbox-blank-circle" size={10} />)
            case 'rejection':
                return (<Icon color={COLORS.rejectionStatus} name="checkbox-blank-circle" size={10} />)
            case 'purchased':
                return (<Icon color={COLORS.buyingStatus} name="checkbox-blank-circle" size={10} />)
            case 'waiting':
                return (<Icon color={COLORS.mainYellow} name="checkbox-blank-circle" size={10} />)
            default:
                return (null)
        }
    }

    return (
        <View style={styles.container}>
            {checkSwitch(status)}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingRight: 10
    },
    logo: {
        width: 8,
        height: 8
    }
})

export default Status;