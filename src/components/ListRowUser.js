import React from 'react';
import { View, Text, StyleSheet, Linking, Platform, } from 'react-native';

import CallButton from './CallButton';
import Status from '~/components/Status';
import * as RootNavigation from '~/navigation/RootNavigation';
import { DeviceConst } from './../constants/constants';
import { inject } from 'mobx-react';
import {
    TouchableRipple
} from 'react-native-paper';
const ListRowUser = (props) => {
    const { topTextLabel, middleTextLabel, bottomTextLabel, status, id, addonAfter, onPressUser, phone } = props;

    const navigate = () => {
        if (props.appStore.can("Э-СтатистикаПоПользователю")) {
            RootNavigation.navigate('UserStack', { post: middleTextLabel, userId: id })
        }
    }

    return (
        <TouchableRipple
            rippleColor="#bdbdbd6b"
            onPress={onPressUser ? () => { onPressUser({ userId: id, userName: middleTextLabel }) } : navigate}>
            <View style={styles.container}>
                <View style={[styles.container_text, { paddingBottom: 5 }]}>
                    <Text style={[styles.worker, styles.padding]}>
                        {topTextLabel}
                    </Text>
                    <View style={[styles.name_container, styles.padding]}>
                        {status ? <Status status={status} /> : null}
                        <Text style={styles.name}>
                            {middleTextLabel}
                        </Text>
                    </View>
                    <Text style={[styles.phone, styles.padding]}>
                        {bottomTextLabel}
                    </Text>
                </View>
                {addonAfter ? <CallButton key={Math.random()} position='center' onPress={() => {
                    (Platform.OS === 'android') ?
                        Linking.openURL(`tel:${phone}`)
                        :
                        Linking.openURL(`telprompt:${phone}`)
                }} /> : null}
            </View>
        </TouchableRipple>
    )
};

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderColor: '#EDF1F7',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
        alignItems: 'center'
    },
    worker: {
        fontSize: 16,
        color: '#A9ABC3',
        lineHeight: 24,
        fontWeight: '400'
    },
    container_text: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    name: {
        fontSize: 20,
        fontStyle: 'normal',
        color: '#000212',
        fontWeight: '700'
    },
    phone: {
        fontSize: 18,
        lineHeight: 24,
        fontWeight: '400',
        color: '#3F3F5C',
    },
    name_container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    padding: {
        marginTop: 5,
        marginBottom: 5,
    }
});

export default inject('appStore')(ListRowUser);