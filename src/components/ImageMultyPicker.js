import * as React from 'react';
import { Image, StyleSheet, ScrollView, TouchableOpacity, View, Alert, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import ImageView from "react-native-image-viewing";
import * as ImagePicker from 'expo-image-picker';
import * as ImageManipulator from 'expo-image-manipulator';
import { inject } from 'mobx-react';
import { COLORS } from '../styles/index';


const ImgPicker = (props) => {
  const { fetchStore, id, photos } = props;
  const [visible, setIsVisible] = React.useState(false);
  const [index, setIndex] = React.useState(0);

  React.useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          Alert.alert('Ошибка', 'Вы не дали прав на просмотр фото');
        }
      }
    })();
  }, []);

  const handlerLongClick = (elem) => {
    return Alert.alert(
      "Внимание!",
      "Вы точно хотите удалить данное фото?",
      [
        {
          text: "Да",
          onPress: () => fetchStore.removePhotoFromCard(elem, id),
        },
        {
          text: 'Отмена',
          style: 'cancel'
        },
      ],
      { cancelable: false }
    );
  };

  const renderPhotoPreview = (photo) => {
    let uri = 'http://appraiser.mediatver.ru/storage/images/card/78x44/';
    uri += photo.slice(0, 1);
    uri += '/' + photo.slice(0, 2);
    uri += '/' + photo.slice(0, 4);
    uri += '/' + photo;
    return uri;
  }

  const renderPhoto = () => {
    let imageArr = [];
    for (let i = 0; i < photos.length; i++) {
      let ur = 'http://appraiser.mediatver.ru/storage/images/card/source/';
      ur += photos[i].slice(0, 1);
      ur += '/' + photos[i].slice(0, 2);
      ur += '/' + photos[i].slice(0, 4);
      ur += '/' + photos[i];
      imageArr.push({ uri: ur })

    }
    return imageArr;
  }

  const openGallery = async () => {

    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: false,
      allowsMultipleSelection: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (result.cancelled === false) {
      let cPhotos = [];
      const pPhoto = await _processImageAsync(result.uri);
      cPhotos.push({
        uri: pPhoto.uri,
        name: pPhoto.uri.split('/').pop(),
        type: 'image/jpeg',
      })


      var body = new FormData();
      body.append("files[]", cPhotos[0]);
      fetchStore.uploadPhoto(body, id);
    }


  }

  const _processImageAsync = async (uri) => {
    const file = await ImageManipulator.manipulateAsync(
      uri,
      [{ resize: { width: 1024 } }],
      { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG }
    );
    return file;
  }



  return (
    <>
      <View style={styles.container}>
        <ScrollView horizontal={true} style={styles.containerPhoto} showsHorizontalScrollIndicator={false}>
          <TouchableOpacity onPress={() => openGallery()} style={styles.button}>
            <Icon name="camera-plus" size={45} color={COLORS.border} />
          </TouchableOpacity>
          <View style={styles.containerPhoto}>
            {photos.map((item, index) => (
              <TouchableOpacity key={Math.random()} onPress={() => { setIsVisible(true); setIndex(index) }} onLongPress={() => handlerLongClick(item)}>
                <Image source={{ uri: renderPhotoPreview(item) }} style={styles.thumbnail} />
              </TouchableOpacity>))}
          </View>
        </ScrollView>
      </View>
      {/* <ImageView
        images={renderPhoto()}
        imageIndex={index}
        visible={visible}
        onRequestClose={() => setIsVisible(false)}
        doubleTapToZoomEnabled={true} /> */}
    </>

  );
}
export default inject('fetchStore')(ImgPicker);

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
  },
  containerPhoto: {
    flexDirection: 'row',

  },

  button: {
    paddingRight: 4,
    borderRadius: 5,
  },

  thumbnail: {
    width: 78,
    height: 44,
    //resizeMode: 'contain',
    borderRadius: 8,
    margin: 4
  }
});
