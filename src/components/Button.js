import React from 'react';
import { StyleSheet } from 'react-native';
import { Button as Btn } from 'react-native-elements';
import { COLORS } from '~/styles';



const Button = (props) => {

  const { label, onPress } = props;

  return (
    <Btn
      onPress={(e) => { onPress(); }}
      title={label}
      titleStyle={styles.btnText}
      buttonStyle={styles.btnTextContainer}
      containerStyle={styles.btnContainer}
      type="clear"
    />
  );
};

const styles = StyleSheet.create({
  btnContainer: {
    width: '100%',
    backgroundColor: COLORS.mainGreen,
    borderRadius: 16,
    height: 56,
    alignContent: 'center'
  },
  btnTextContainer: {
    width: '100%',
    height: 56,
    borderRadius: 16,
    backgroundColor: COLORS.mainGreen,
  },
  btnText: {
    alignItems: 'center',
    color: COLORS.mainBg,
    fontWeight: 'bold',
    fontSize: 18
  }
});

export default Button;
