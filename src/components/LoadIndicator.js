import React from 'react';
import { StyleSheet, ActivityIndicator, View, Text } from 'react-native';
import { COLORS } from '~/styles';
import Empty from './../../svg/empty.svg';

const LoadIndicator = (props) => {
    const { type } = props;
    return (
        <View style={styles.container}>
            {type == 'empty' ?
                <>
                    <Empty />
                    <Text style={{ fontSize: 16, color: "#d9d9d9" }}>Нет данных</Text>
                </>
                :
                <ActivityIndicator color={COLORS.mainGreen} size="large" />}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default LoadIndicator;
