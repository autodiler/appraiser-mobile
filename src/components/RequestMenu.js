
import React from 'react';
import { Dimensions, Text, View, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { observer } from 'mobx-react-lite';
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

const RequestMenu = observer((props) => {

    const { dataList, visible, closable, onClose } = props;

    return (
        <Modal
            onBackdropPress={onClose}
            deviceWidth={deviceWidth}
            deviceHeight={deviceHeight}
            isVisible={visible}
            onStartShouldSetResponder={() => { return true }}
            style={[styles.modalContainer]}>
            <View renderToHardwareTextureAndroid={true} style={styles.container}>
                {closable ?
                    <TouchableOpacity delayPressIn={0.3} style={styles.icon} onPress={onClose}>
                        <MaterialCommunityIcons name="close" size={20} />
                    </TouchableOpacity>
                    : null}
                <View onStartShouldSetResponder={() => { return false }} style={[styles.flatList]}>
                    <FlatList
                        keyboardShouldPersistTaps="handled"
                        keyExtractor={(item, index) => index.toString()}
                        data={dataList}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity delayPressIn={0.3} onStartShouldSetResponder={() => { return true }} onPress={() => { onClose(); item.onPress() }} style={styles.itemList} >
                                    <Text style={styles.item}>{item.title}</Text>
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            </View>
        </Modal>
    );
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    flatList: {
        flex: 0.9,
        alignContent: 'center'
    },
    itemList: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    item: {
        padding: 10,
        marginTop: 10,
        fontSize: 16,
        fontWeight: '400',
    },
    icon: {
        position: 'absolute',
        right: 10,
        zIndex: 100
    },
    modalContainer: {
        width: deviceWidth - 16,
        marginTop: 30,
        marginLeft: 8,
        marginRight: 8,
        marginBottom: 8,
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#ffffff',
        borderRadius: 16,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 22,
        paddingBottom: 26
    },

});

export default RequestMenu;