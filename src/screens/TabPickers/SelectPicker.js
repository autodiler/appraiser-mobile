import * as React from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';
import Modal from 'react-native-modal';
import ListPickers from '~/components/ListPickers';
import Button from '~/components/Button';
import Input from '~/components/Input';
import * as RootNavigation from '~/navigation/RootNavigation';
import { inject, observer } from 'mobx-react';
import { COLORS } from '~/styles';
import { DeviceConst } from './../../constants/constants';
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

@inject('fetchStore', 'pickerStore', 'overallStore')
@observer
class SelectPicker extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      confirmModal: false,
      name: null,
      phone: null,
      price: null,
      comment: null
    }

  }

  componentDidMount() {
    const { pickerStore } = this.props;
    pickerStore.refreshList();
  }

  applySelectPicker = async () => {
    const { fetchStore, navigation, route } = this.props;
    const { card_id } = route.params;
    let textM = `Подборщик:
${this.state.name}
#${this.state.phone}
${this.state.price}₽
${this.state.comment}`;
    let newMessage = {
      "card_id": card_id,
      "message": textM
    }
    await fetchStore.createMessage(newMessage).then(res => {
      fetchStore.getMessages(card_id);
    });

    navigation.goBack();
  }
  onSelectPicker = ({ m_comment, m_name, m_phone, m_price }) => {
    this.setState({ confirmModal: true, comment: m_comment, name: m_name, phone: m_phone, price: m_price })
  }


  render() {
    const { setSearchString, filter } = this.props.pickerStore;
    const { pickerStore } = this.props;
    const size = {
      height: 56,
    }

    const { onlyList } = this.props.route.params;
    
    return (
      <View style={styles.container}>
        <View style={styles.searchView}>
          <Input
            onChange={(text) => { setSearchString(text); }}
            value={filter.name}
            placeholder={"Найти подборщика"}
            {...size} />
        </View>
        <ListPickers
          itemList={pickerStore.getPickersList}
          onPressPicker={onlyList ? null : this.onSelectPicker} />
        {onlyList ? null : <Modal
          onBackdropPress={() => { this.setState({ confirmModal: false }) }}
          deviceWidth={deviceWidth}
          deviceHeight={deviceHeight}
          isVisible={this.state.confirmModal}
          style={styles.confirm_modal}
          onStartShouldSetResponder={() => { return true }}
        >
          <View style={styles.confirm} >
            <Text style={styles.confirm_text}>Выбрать {`${this.state.name}`}?</Text>
            <View style={styles.confirm_button}>
              <View style={styles.confirm_button_item}>
                <Button label="Да" onPress={() => this.applySelectPicker()} />
              </View>
              <View style={styles.confirm_button_item}>
                <Button label="Отмена" onPress={() => this.setState({ confirmModal: false })} />
              </View>
            </View>
          </View>
        </Modal>}

      </View>
    )
  }
};

export default SelectPicker;

const padding = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1,
    backgroundColor: COLORS.mainBg
  },
  confirm_modal: {
    padding: 0,
    margin: 10,
    width: deviceWidth - 20,
  },
  confirm: {
    borderRadius: 18,
    backgroundColor: 'white',
    padding: 20,
    alignItems: 'center'
  },
  confirm_text: {
    fontSize: 20,
    fontWeight: 'bold'
  },
  confirm_button: {
    width: '100%',
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'space-around'
  },
  confirm_button_item: {
    width: '48%',
  },
  searchView: {
    padding: padding,
    justifyContent: 'flex-start'
  }
});