import * as React from 'react';
import { View, StyleSheet, Alert} from 'react-native';
import Input from '~/components/Input';
import MInput from '~/components/MultiLineInput';
import Button from '~/components/Button';
import * as RootNavigation from '~/navigation/RootNavigation';
import { COLORS } from '~/styles';
import { DeviceConst } from './../../constants/constants';
import { inject } from 'mobx-react';

const NewPickerStack = (props) => {

    const { fetchStore, pickerStore, navigation } = props;   

    const [formValue, setFormValue] = React.useState({})
    React.useEffect(() => {
        setFormValue({
            name: '',
            phone: '+7',
            price: '',
            comment: '',
            city : "Тверь"
        })
        
    }, [])
    const handleChange = (name, value) => {
        setFormValue(prevState => ({
            ...prevState,
            [name]: value
        }));
      }
      const submitForm = () => {

        const {name, phone, comment, price} = formValue;

        if (!!name && !!phone && !!price ) {
            fetchStore.creatPicker(formValue);
            pickerStore.refreshList();
            navigation.goBack()
            return
          }
        return Alert.alert(
            "Ошибка!",
            "Заполнены не все данные!",
            [
                { text: "OK" }
            ],
            { cancelable: false }
        );
    }  
    const size = {
        height: 56,
        margin: {
            marginTop: 16
        }
    }
    return (
        <View style={styles.container}>

            <Input
                onChange={(text) => handleChange("name", text)}
                value={formValue.name}
                label="ФИО"
                {...size} />        
            <Input
                onChange={(text) => { handleChange("phone", text) }}
                value={formValue.phone}
                textContentType='telephoneNumber'
                dataDetectorTypes='phoneNumber'
                keyboardType='phone-pad'
                label="Телефон" 
                {...size}/>            
            <Input
                onChange={(text) => { handleChange("price", text) }}
                value={formValue.price}
                keyboardType='numeric'
                label="Тариф" 
                {...size}/>
            <MInput
                onChange={(text) => { handleChange("comment", text) }}
                value={formValue.comment}
                label="Описание" 
                {...size}/>
            <View style={styles.buttonView}>
                <Button label="Добавить" onPress={() => submitForm()} />
            </View>    
            
        </View>
    )
};

export default inject('fetchStore', 'pickerStore','overallStore')(NewPickerStack);

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(4);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.mainBg,
        alignItems: 'center',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight
    },
    buttonView: {
        width: '100%',
        paddingTop: 16
    },
});
