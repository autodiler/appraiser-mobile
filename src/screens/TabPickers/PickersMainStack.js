import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import ListPickers from '~/components/ListPickers';
import AddButton from '~/components/AddButton';
import * as RootNavigation from '~/navigation/RootNavigation';
import { inject, observer } from 'mobx-react';
import { COLORS } from '~/styles';
import Input from '~/components/Input';
import { DeviceConst } from './../../constants/constants';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('fetchStore', 'pickerStore', 'overallStore')
@observer
class PickersMainStack extends React.Component {
  constructor(props) {
    super(props)

  }

  componentDidMount() {
    const { pickerStore } = this.props;
    pickerStore.refreshList();
  }


  render() {
    const { setSearchString, filter } = this.props.pickerStore;
    const { pickerStore } = this.props;
    const size = {
      height: 56,
    }

    return (
      <View style={styles.container}>
        <View style={styles.searchView}>
          <Input
            onChange={(text) => { setSearchString(text); }}
            value={filter.name}
            placeholder={"Найти подборщика"}
            {...size} />
        </View>
        <ListPickers itemList={pickerStore.getPickersList} />
        <AddButton
          onPress={() => { RootNavigation.navigate('NewPickerStack') }}
          icon={<Icon
            name="account-plus"
            size={24}
            color="white"
          />}
          label="+" />
      </View>
    )
  }
};

export default PickersMainStack;

const padding = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    paddingTop: 24,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flex: 1,
    backgroundColor: COLORS.mainBg
  },
  searchView: {
    padding: padding,
    justifyContent: 'flex-start'
  }
});