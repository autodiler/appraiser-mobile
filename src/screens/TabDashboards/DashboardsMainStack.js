import * as React from 'react';
import { View } from 'react-native';
import { STYLES } from '~/styles';
import { inject, observer } from 'mobx-react';
import Filters from '~/components/Filters';

import DasboardsStack from './DashboardsStack';
import DashboardsFilterStack from './DashboardsFilterStack';
import { createStackNavigator } from '@react-navigation/stack';
import * as RootNavigation from '~/navigation/RootNavigation';

import SafeAreaHooks from '~/components/SafeAreaHooks';

const Stack = createStackNavigator();

@inject('fetchStore', 'usersTabStore', 'dataStore', 'dashboardsStore')
@observer
class DashboardsMainStack extends React.Component {

  constructor() {
    super()
  }

  render() {
    const { dashboardsStore } = this.props;

    const { getDashboardsFilter } = dashboardsStore;

    const onPressFilter = (route, index, filter) => {
      dashboardsStore.setDashboardsFilter(filter, index);
      RootNavigation.navigate(route)
    }

    return (
      <>
        <View style={STYLES.barStyle}>
          <Filters
            filters={getDashboardsFilter}
            onPress={onPressFilter}
            hide={true} />
        </View>
        {/* <View style={styles.logoView}>
             <Logo width={'40%'} style={styles.logo} />
           </View> */}
        <Stack.Navigator screenOptions={{
          headerShown: false,
          animationEnabled: false
        }} initialRouteName="Dashboards">
          <Stack.Screen name="Dashboards" component={DasboardsStack} />
          <Stack.Screen name="DashboardsTown" component={DashboardsFilterStack} />
          <Stack.Screen name="DashboardsPickers" component={DashboardsFilterStack} />
        </Stack.Navigator>
      </>
    )
  }
};

export default SafeAreaHooks(DashboardsMainStack);

