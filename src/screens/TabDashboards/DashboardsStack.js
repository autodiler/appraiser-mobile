import * as React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Status from '~/components/Status';
import { inject, observer } from 'mobx-react';
import { COLORS } from '~/styles';
import Button from '~/components/Button';
import DashboardsCard from '~/components/DashboardsCard';
import * as RootNavigation from '~/navigation/RootNavigation';
import { DeviceConst } from './../../constants/constants';


@inject('fetchStore', 'usersTabStore', 'dataStore', 'dashboardsStore', 'appStore')
@observer
class DashboardsStackView extends React.Component {
    constructor(props) {
        super(props)
    }


    navigate(filter) {
        const { dataStore } = this.props;
        const { setFilterStatus } = dataStore;
        setFilterStatus(filter, dataStore.filters.indexOf(dataStore.getFilter(filter)));
        RootNavigation.navigate('Заявки', { screen: "RequestsMainStack", params: { filter: "new" } })
    }

    render() {

        const { usersTabStore, dashboardsStore, fetchStore, appStore } = this.props;
        const onlineCount = usersTabStore.filters[1].count;
        const { cardsCount } = dashboardsStore;

        return (
            <View style={styles.container}>
                <View style={styles.container_text}>
                    <Status status='online' />
                    <Text style={styles.containerUsers}>
                        {onlineCount} пользователей онлайн
                    </Text>
                </View>
                <View style={styles.cardContainer}>
                    <DashboardsCard
                        text="Новые"
                        padding={{ paddingRight: 4 }}
                        status="new"
                        title={cardsCount.new}
                        color="#EBF7FF"
                        onPress={() => { this.navigate("new") }} />
                    <DashboardsCard
                        text="В работе"
                        padding={{ paddingLeft: 4 }}
                        status="in-work"
                        title={cardsCount.inwork}
                        color="#EBFCF6"
                        onPress={() => { this.navigate("inwork") }} />
                    <DashboardsCard
                        text="Выкуплено"
                        padding={{ paddingRight: 4 }}
                        status="buying"
                        title={cardsCount.purchased}
                        color="#F6EBFF"
                        onPress={() => { this.navigate("purchased") }} />
                    <DashboardsCard
                        text="Отказ"
                        padding={{ paddingLeft: 4 }}
                        status="expired"
                        title={cardsCount.rejection}
                        color="#FFF0F4"
                        onPress={() => { this.navigate("rejection") }} />
                </View>
                <View style={styles.btnContainer}>
                    {appStore.can('С-Пользователи-Добавление') ? <Button onPress={() => { RootNavigation.navigate('NewRequestStack') }} label="Новая заявка" /> : null}
                </View>
            </View >
        )
    }
}

const margin = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.mainBg
    },
    cardContainer: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        flex: 1,
        marginLeft: margin,
        marginRight: margin,
        marginBottom: 16,
    },
    btnContainer: {
        marginLeft: margin,
        marginRight: margin,
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: DeviceConst.pixelRatio.getPixelSizeForLayoutSize(14)
    },

    container_text: {
        // marginLeft: margin,
        // marginRight: margin,
        // marginBottom: margin,
        // backgroundColor: 'red',
        marginTop: 16,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    containerUsers: {
        fontSize: 18,
        fontWeight: 'bold'
    }
});


export default DashboardsStackView;