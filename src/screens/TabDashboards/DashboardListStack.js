import * as React from 'react';
import { View, StyleSheet, ScrollView, Text } from 'react-native';
import PeriodButton from '~/components/PeriodButton';
import ListRequests from '~/components/ListRequests';
import { COLORS } from '~/styles';


const mState = [
  {
    title: 'Ford Focus 2015 года с Avito',
    name: 'Александр Златоусов ',
    date: 1603834749,
    status: 'new',
    urgently: false,
    delay: false

  },
  {
    title: 'Ford Focus 2012 ',
    name: 'Петр Вакунин ',
    date: 1603824749,
    status: 'expired',
    urgently: false,
    delay: true
  },
  {
    title: 'Kia Rio 2018 ',
    name: 'Александр Златоусов ',
    date: 1603824749,
    status: 'online',
    urgently: true,
    delay: false
  },
  {
    title: 'Лада Калина 2008 года (частник) ',
    name: 'Нет исполнителя ',
    date: 1603824749,
    status: 'online',
    urgently: false,
    delay: true
  }
];

export default DashboardListStack = () => {
  return (
    <View style={styles.container}>
      <ScrollView horizontal={true} style={styles.barStyle} showsHorizontalScrollIndicator={false}>
        <BarrelCount title='Все' counter={22} colorBG='#00BA88' colorText='white' />
        <BarrelCount title='В работе' state='online' counter={12} colorBG={COLORS.lightGreen} colorText='black' />
        <BarrelCount title='Выкуплено' state='buying' counter={10} colorBG='#F6EBFF' colorText='black' />
        <BarrelCount title='Отказ' state='buying' counter={10} colorBG='#FFF0F4' colorText='black' />
      </ScrollView>
      <View style={styles.periodContainer}>
        <PeriodButton />
      </View>
      <ListRequests itemList={mState} />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flex: 1,
    backgroundColor: COLORS.mainBg
  },
  barStyle: {
    maxHeight: 50,
    flexDirection: 'row',
    marginTop: 8,
    paddingLeft: 8,


  },
  periodContainer: {
    width: '100%',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    marginLeft: 16

  },
  listContainer: {
    height: 200,
    width: '100%'
  }
});
