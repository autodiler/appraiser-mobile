import * as React from 'react';
import { View, StyleSheet, Text, FlatList, TouchableHighlight, RefreshControl } from 'react-native';
import { COLORS } from '~/styles';
import { inject, observer } from 'mobx-react'
import LoadIndicator from '~/components/LoadIndicator';
import { DeviceConst } from './../../constants/constants';

const margin = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

@inject('dashboardsStore', 'fetchStore', 'dataStore')
@observer
class DashboardsFilterStack extends React.Component {
  constructor(props) {
    super(props)


  }

  componentDidMount() {
    const { fetchStore, navigation, dashboardsStore } = this.props;
    
    navigation.addListener('focus', (e) => {
      fetchStore.getStatistic();
    });

    navigation.addListener('blur', (e) => {
      dashboardsStore.setStatisticPage(1);
      dashboardsStore.clearStatistic();
    });
  }

  loadMore = () => {
    const { setStatisticPage } = this.props.dashboardsStore;
    const { fetchStore } = this.props;
    setStatisticPage();
    fetchStore.getStatistic();
  }

  refresh = () => {
    const { setStatisticPage, clearStatistic } = this.props.dashboardsStore;
    const { fetchStore } = this.props;
    setStatisticPage(1);
    clearStatistic();
    fetchStore.getStatistic();
  }


  renderItem = ({ item }) => {
    const { dataStore } = this.props;
    let count = null;
    return (
      <TouchableHighlight activeOpacity={0.8} underlayColor="#DDDDDD">
        <View style={styles.row_container}>
          <View style={[styles.container_text, { paddingBottom: 5 }]}>
            <View key={styles.name} style={[styles.name_container, styles.padding]}>
              <Text style={styles.name}>
                {item.title}
              </Text>
            </View>
            <View style={styles.info_item_container}>
              {Object.keys(item).map((stat, index) => {
                if (stat != 'title') {
                  let status = dataStore.getStatusByCode(stat);
                  count = count + +item[stat];
                  return (
                    <Text key={index} style={styles.info_item}>
                      {`${status.title}: `}
                      <Text style={[styles.info_item_count, { color: status.color }]}>
                        {item[stat]}
                      </Text>
                    </Text>
                  )
                }
              })}
              <Text style={styles.info_item}>
                {"Всего: "}
                <Text style={[styles.info_item_count, { color: COLORS.mainGreen }]}>
                  {count}
                </Text>
              </Text>
            </View>
          </View>
          <View style={styles.total_count}>
            <Text style={styles.total_count_text}>{item.total}</Text>
          </View>
        </View>
      </TouchableHighlight>
    )
  }

  render() {
    const { statistic, statPage, statisticPageCount } = this.props.dashboardsStore;

    return (
      <View style={styles.container}>
        {statistic.length == 0 ?
          <LoadIndicator /> :
          <FlatList
            refreshControl={<RefreshControl onRefresh={this.refresh} />}
            data={statistic}
            onEndReached={statisticPageCount == statPage ? null : this.loadMore}
            onEndReachedThreshold={0.5}
            scrollEventThrottle={160}
            keyExtractor={(item, index) => `${'key' + index}`}
            renderItem={this.renderItem}
          />}
      </View>
    )
  }
}

export default DashboardsFilterStack;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.mainBg,
    borderColor: '#EDF1F7',
  },
  row_container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderColor: '#EDF1F7',
    paddingLeft: margin,
    paddingRight: margin,
    alignItems: 'center'
  },
  container_text: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  name: {
    fontSize: 20,
    fontStyle: 'normal',
    color: '#000212',
    fontWeight: '700'
  },
  total_count: {

  },
  total_count_text: {
    fontSize: 46
  },
  info_item_container: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10
  },
  info_item_count: {
    marginRight: 10,
    fontWeight: 'bold'
  },
  info_item: {
    fontSize: 18,
    lineHeight: 24,
    fontWeight: '400',
    color: '#3F3F5C',
    marginRight: 15

  },
  name_container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  padding: {
    marginTop: 5,
    marginBottom: 5,
  }
});
