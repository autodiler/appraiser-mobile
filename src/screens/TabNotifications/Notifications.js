import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import ListUsers from '~/components/ListUsers';
import SafeAreaHooks from '~/components/SafeAreaHooks';

const data = [
  {
    datetime: "Сегодня  12:27",
    title: "Ford Focus 2015 года",
    action: "Добавлен новый комментарий",
    cardId: 25
  },
  {
    datetime: "Сегодня  15:59",
    title: "Skoda Rapid 2010 года",
    action: "Добавлен новый комментарий",
    cardId: 25
  }, {
    datetime: "Вчера  10:23",
    title: "Nissan Terano 2020 года",
    action: "Изменен статус заявки",
    cardId: 25
  }, {
    datetime: "Вчера  12:27",
    title: "Ford Mondeo 2010 года",
    action: "Заявка принята в работу",
    cardId: 25
  }, {
    datetime: "Вчера  05:55",
    title: "Ford Focus 2015 года",
    action: "Заявка закрыта",
    cardId: 25
  }, {
    datetime: "Вчера  18:30",
    title: "Chevrolet Niva 208 года",
    action: "Добавлен новый комментарий",
    cardId: 25
  }, {
    datetime: "Вчера  12:27",
    title: "Ford Focus 2015 года",
    action: "Добавлен новый комментарий",
    cardId: 25
  }, {
    datetime: "28.11.20  14:46",
    title: "Ferrari Aventador 2018 года",
    action: "Заявка принята в работу",
    cardId: 25
  }
]

export const Notifications = () => {

  const refreshList = () => {
    console.log(1)
  }

  return (
    <View style={styles.constainer}>
      <ListUsers
        topTextLabel="datetime"
        middleTextLabel="title"
        bottomTextLabel="action"
        addonAfter={false}
        // currentPage={page}
        // pageCount={pageCount}
        itemList={data}
        // loadMore={fetchStore.getUsersListData}
        // setPage={setPage}
        // refreshing={refreshing}
        //loading={loading}
        onRefresh={() => refreshList()}
        mode='user'
      // setLoading={setLoading} 
      />
    </View>
  );
};

export default SafeAreaHooks(Notifications);

const styles = StyleSheet.create({
  constainer: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});
