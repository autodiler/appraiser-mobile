import * as React from 'react';
import * as RootNavigation from '~/navigation/RootNavigation';
import { inject } from 'mobx-react';
import { COLORS } from '~/styles';
import { DeviceConst } from './../../constants/constants';

import { View, SafeAreaView, StyleSheet } from 'react-native';
import {
    Title,
    Text,
    TouchableRipple,
} from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const padding = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const OptionsStack = (props) => {

    const menu = [
        { route: "Статистика", icon: "chart-pie", post: null },
        { route: "Заявки", icon: "format-list-bulleted", post: null },
        { route: "Уведомления", icon: "bell-ring-outline", post: null },
        { route: "Пользователи", icon: "account-supervisor", post: null },
        { route: "SelectPicker", title: "Контакты подборщиков", icon: "account-details", post: { post: 'Список подборщиков', onlyList: true } },
        { route: "AboutApp", title: "О приложеннии", icon: "information-variant", post: null }
    ]

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.userInfoSection}>
                <View style={{ flexDirection: 'row', marginTop: 0 }}>
                    <Title style={[styles.title, {
                        marginTop: 15,
                        marginBottom: 5,
                    }]}>Меню</Title>
                </View>
            </View>
            <View style={styles.menuWrapper}>
                {menu.map((item, index) => {
                    return (
                        <TouchableRipple key={index} onPress={() => { RootNavigation.navigate(item.route, item.post) }}>
                            <View style={styles.menuItem}>
                                <Icon name={item.icon} color={COLORS.mainGreen} size={25} />
                                <Text style={styles.menuItemText}>{item.title || item.route}</Text>
                            </View>
                        </TouchableRipple>
                    )
                })}
                <TouchableRipple onPress={() => { props.appStore.logout(); }}>
                    <View style={styles.menuItem}>
                        <Icon name="logout" color={COLORS.mainGreen} size={25} />
                        <Text style={styles.menuItemText}>Выход</Text>
                    </View>
                </TouchableRipple>
            </View>
        </SafeAreaView>
    );
};

export default inject("userStore", "appStore")(OptionsStack);


const styles = StyleSheet.create({
    container: {
        display: 'flex',
        paddingTop: 24,
        flex: 1,
        backgroundColor: COLORS.mainBg
    },
    userInfoSection: {
        paddingHorizontal: padding,
        marginBottom: 10,
        flexDirection: 'row',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    caption: {
        fontSize: 14,
        lineHeight: 14,
        fontWeight: '500',
    },
    row: {
        flexDirection: 'row',
        marginBottom: 10,
        marginRight: 10
    },
    infoBoxWrapper: {
        borderBottomColor: '#dddddd',
        borderBottomWidth: 1,
        borderTopColor: '#dddddd',
        borderTopWidth: 1,
        flexDirection: 'row',
        height: 100,
    },
    infoBox: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    menuWrapper: {
        borderTopColor: '#dddddd',
        borderTopWidth: 1,
    },
    menuItem: {
        flexDirection: 'row',
        paddingVertical: 15,
        paddingHorizontal: padding,
    },
    menuItemText: {
        color: '#777777',
        marginLeft: padding,
        fontWeight: '600',
        fontSize: 16,
        lineHeight: 26,
    },
});