import * as React from 'react';
import { Platform, StyleSheet, View, Text, TouchableOpacity, Linking, Alert, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import { DeviceConst } from './../../constants/constants';


import ImageMultyPicker from '~/components/ImageMultyPicker';



import Chat from '~/components/Chat';
import CityButton from '~/components/CityButton';
import AutotekaButton from '~/components/atotekaButton';
import StatusButton from '~/components/StatusButton';
import { COLORS } from '~/styles';
import { inject, observer } from 'mobx-react';

import RequestMenu from '~/components/RequestMenu';
import { Modal } from 'react-native-paper';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


const RequestStack = (props) => {

    const [menuVisible, setMenuVisible] = React.useState(false);
    const [start, setStart] = React.useState(true);
    const [headerTitle, setHeaderTitle] = React.useState('')

    const { dataStore, route, navigation } = props;

    const { author_name, executor_name, urgency, city_title, status_title, url, photos, status_id, autoteka } = dataStore.getCardData;

    const getMenu = () => {
        return (
            [
                {
                    title: 'Выбор подборщика',
                    onPress: () => {
                        props.navigation.navigate('SelectPicker', { post: "Выбор подборщика", card_id: props.route.params.id, onlyList: false })
                    }
                },
                {
                    title: 'Передать карточку другому оценщику',
                    onPress: () => {
                        props.navigation.navigate('SelectUser', { card_id: props.route.params.id })
                    }
                }
            ]
        )
    }

    React.useEffect(() => {
        dataStore.refreshCard(route.params.id, setHeaderTitle);
        navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity delayPressIn={0.3} onPress={() => { setMenuVisible(true) }}>
                    <Icon name='dots-vertical' size={30} color="#a9abc3" />
                </TouchableOpacity>
            ),
            headerRightContainerStyle: {
                marginRight: 10
            }
        });

        setTimeout(() => setStart(false), 700);

        props.navigation.setOptions({
            title: headerTitle,
        });

    }, [headerTitle, props.navigation, route.params.id]);


    const handleClick = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                return Alert.alert(
                    url,
                    "Данная ссылка не валидна!",
                    [
                        { text: "OK" }
                    ],
                    { cancelable: false }
                );
            }
        });
    };




    let tempPhotos = [];
    if (photos) { tempPhotos = JSON.parse(photos) }
    let pdfFileName = null;
    if (autoteka) { pdfFileName = JSON.parse(autoteka) }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : ""}
            keyboardVerticalOffset={90}
            style={{ flex: 1 }} //this value is depends upon your view/component height
        >
            <View style={styles.maincontainer}>
                {!dataStore.expandable ?
                    <>
                        <View style={styles.containerName}>
                            <Text style={styles.textTitle}>
                                Автор:
                            </Text>
                            <Text>
                                {author_name}
                            </Text>
                        </View>
                        <View style={styles.containerName}>
                            <Text style={styles.textTitle}>
                                Оценщик:
                                </Text>
                            <Text>
                                {executor_name == null ? 'Нет исполнителя' : executor_name}
                            </Text>
                        </View>
                        {Platform.OS != 'web' ? <View style={styles.containerPhoto}>
                            <ImageMultyPicker id={route.params.id} photos={tempPhotos} />
                        </View> : null}

                        <View style={styles.containerCity}>
                            <CityButton style={{ marginRight: 8 }} cityName={city_title} />
                            {urgency ?
                                <View style={styles.force}>
                                    <Text>Срочно</Text>
                                    <Icon name="lightning-bolt-outline" color={COLORS.mainRed} size={20} />
                                </View> :
                                <View style={styles.force}>
                                    <Text>Срочно</Text>
                                    <Icon name="lightning-bolt-outline" color={COLORS.border} size={20} />
                                </View>}

                        </View>
                        <View style={styles.containerCity}>
                            <AutotekaButton id={route.params.id} pdf={pdfFileName} style={{ marginRight: 8 }} />
                            <TouchableOpacity onPress={() => handleClick(url)} style={styles.force}>
                                <Text style={{ marginRight: 5 }}>Сайт</Text>
                                <Icon name="link-variant" size={18} color={COLORS.mainGreen} style={{ paddingTop: 3 }} />
                            </TouchableOpacity>
                            <StatusButton card_id={route.params.id} status={status_title} status_id={status_id} />
                        </View>
                    </> : null}
                <TouchableOpacity delayPressIn={0.3} style={styles.containerIndicator} onPress={() => dataStore.setExpandable()}>
                    <Icon name="minus" size={40} color={COLORS.default} />
                </TouchableOpacity>
                <View style={styles.containerChat}>
                    <Chat cardId={route.params.id} executor={executor_name} author={author_name} navigation={navigation} />
                </View>
                <RequestMenu dataList={getMenu()} visible={menuVisible} onClose={() => { setMenuVisible(false) }} />
                <Modal visible={start} transparent={false}>
                    <View style={styles.containerSpinner}>
                        <ActivityIndicator size="large" color="#00BA88" />
                    </View>
                </Modal>
            </View>
        </KeyboardAvoidingView >

    )

};



const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(4);

const styles = StyleSheet.create({
    containerSpinner: {
        width: '100%',
        height: '110%',
        justifyContent: "center",
        backgroundColor: 'grey'
    },
    container: {
        backgroundColor: COLORS.mainBg,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
        //paddingTop: marginLeftRight
    },
    force: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignContent: 'center',
        borderRadius: 8,
        paddingLeft: 10,
        paddingRight: 10,
        backgroundColor: COLORS.defaultFilter, height: '100%',
        marginRight: 8
    },
    maincontainer: {
        flex: 1,
        backgroundColor: COLORS.mainBg,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    containerIndicator: {
        height: 20,
        width: '100%',
        padding: 15,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    containerName: {
        width: '100%',
        height: 20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
    },
    containerCity: {
        width: '100%',
        height: 32,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 8,
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,

    },
    textTitle: {
        color: COLORS.default,
        marginRight: 6
    },
    containerPhoto: {
        width: '100%',
        marginTop: 4,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight,
    },
    containerChat: {
        width: '100%',
        backgroundColor: '#E5E5E5',
        flex: 1
    }
});

export default inject('fetchStore', 'dataStore', 'overallStore')(observer(RequestStack));