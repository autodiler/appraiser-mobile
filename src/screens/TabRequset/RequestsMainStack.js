import * as React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import AddButton from '~/components/AddButton';
import { COLORS, STYLES } from '~/styles';
import * as RootNavigation from '~/navigation/RootNavigation';
import ListRequests from '~/components/ListRequests';
import CityPicker from '~/components/CityPicker';
import Tfilter from '~/components/Tfilter';
import { inject, observer } from 'mobx-react';
import { DeviceConst } from '~/constants/constants';
import Filters from '~/components/Filters';

import SafeAreaHooks from '~/components/SafeAreaHooks';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('fetchStore', 'dataStore', 'overallStore')
@observer
class RequestsMainStack extends React.Component {
  constructor(props) {
    super(props)


  }

  componentDidMount() {
    const { dataStore } = this.props;
    dataStore.refreshList();
  }


  render() {

    const { getCardsListData, setPage, loading, pageCount, page, refreshing, setFilterStatus, refreshList, filters, loadMore } = this.props.dataStore;

    return (
      <>
        <View style={[styles.barStyle, { paddingRight: 6 }]}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Filters filters={filters} onPress={setFilterStatus} />
          </ScrollView>
        </View>
        {/* <View style={styles.containerFilter}>
          <Tfilter />
          <CityPicker />
        </View> */}
        <ListRequests
          itemList={getCardsListData}
          currentPage={page}
          pageCount={pageCount}
          loadMore={loadMore}
          setPage={setPage}
          refreshing={refreshing}
          loading={loading}
          onRefresh={refreshList} />
        <AddButton
          onPress={() => { RootNavigation.navigate('NewRequestStack'); }}
          icon={<Icon
            name="car-multiple"
            size={24}
            color="white"
          />}
          label="+" />
      </>
    )
  }
};

export default SafeAreaHooks(RequestsMainStack);

const margin = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: COLORS.mainBg,
  },
  containerFilter: {
    width: '100%',
    flexDirection: 'row',
    height: 32,
    marginTop: 6,
    marginBottom: 16,
    paddingLeft: margin,
    paddingRight: margin,
  },
  barStyle: {
    paddingLeft: margin,
    marginLeft: -8,
    flexDirection: 'row',
  },
});
