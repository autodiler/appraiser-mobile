import * as React from 'react';
import { StyleSheet, View, Text, Button, TouchableHighlight, ActivityIndicator } from 'react-native';
import { COLORS } from '~/styles';
import { DeviceConst } from './../../constants/constants';
import { inject, observer } from 'mobx-react';
import * as ImageManipulator from 'expo-image-manipulator';
import { ImageBrowser } from 'expo-image-picker-multiple';



@inject('fetchStore', 'navigation')
@observer
class ChoosePhotoStack extends React.Component {
  constructor(props) {
    super(props);
    this.cPhotos = [];
    const { route } = this.props;
    this.routeId = route.params.id;

  }

  componentDidMount() {
    const { navigation, fetchStore } = this.props;

    navigation.setOptions({
      headerRight: () => (
        <TouchableHighlight
          style={styles.submitG}
          onPress={() => { fetchStore.uploadPhoto(this.setFile(), this.routeId); navigation.goBack(); }}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Выбрать</Text>
        </TouchableHighlight>
      ),
      headerLeft: () => (
        <TouchableHighlight
          style={styles.submitR}
          onPress={() => navigation.goBack()}
          underlayColor='#fff'>
          <Text style={styles.submitText}>Отмена</Text>
        </TouchableHighlight>
      ),
    });
  }

  setFile = () => {
    var body = new FormData();
    for (let i = 0; i < this.cPhotos.length; i++) {
      body.append("files[" + i + "]", this.cPhotos[i]);
    }

    return body;
  }

  imagesCallback = (callback) => {
    callback.then(async (photos) => {
      this.cPhotos = [];
      for (let photo of photos) {
        const pPhoto = await this._processImageAsync(photo.localUri);
        this.cPhotos.push({
          uri: pPhoto.uri,
          name: pPhoto.uri.split('/').pop(),
          type: 'image/jpeg',
        })

      }
    })
      .catch((e) => console.log(e))
    // .finally(() => navigation.setParams({ loading: false }));
  };

  async _processImageAsync(uri) {
    const file = await ImageManipulator.manipulateAsync(
      uri,
      [{ resize: { width: 1024 } }],
      { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG }
    );
    return file;
  }

  updateHandler = (count, onSubmit) => {

  };

  renderSelectedComponent = (number) => (
    <View style={styles.countBadge}>
      <Text style={styles.countBadgeText}>{number}</Text>
    </View>
  );

  render() {
    const emptyStayComponent = <Text style={styles.emptyStay}>Empty =(</Text>;

    return (
      <View style={[styles.flex, styles.container]}>
        <ImageBrowser
          max={8}
          onChange={this.updateHandler}
          callback={this.imagesCallback}
          renderSelectedComponent={this.renderSelectedComponent}
          emptyStayComponent={emptyStayComponent}
        />
      </View>
    );
  }

}

export default ChoosePhotoStack;

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.mainBg,
    alignItems: 'center',

  },
  countBadge: {
    paddingHorizontal: 8.6,
    paddingVertical: 5,
    borderRadius: 50,
    position: 'absolute',
    right: 3,
    bottom: 3,
    justifyContent: 'center',
    backgroundColor: COLORS.mainGreen
  },
  countBadgeText: {
    fontWeight: 'bold',
    alignSelf: 'center',
    padding: 'auto',
    color: 'white'
  },
  submitG: {
    marginRight: 20,
    padding: 8,
    backgroundColor: '#00BA88',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#fff'
  },
  submitR: {
    marginLeft: 20,
    padding: 8,
    backgroundColor: '#FF3D71',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#fff'
  },
  submitText: {
    color: '#fff',
    textAlign: 'center',
  }
});
