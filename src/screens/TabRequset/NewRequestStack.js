import * as React from 'react';
import { StyleSheet, View, Text, Alert } from 'react-native';
import Button from '~/components/Button';
import Input from '~/components/Input';
import DismissKeyboard from '~/components/DismissKeyboard';
import { COLORS } from '~/styles';
import { DeviceConst } from './../../constants/constants';
import { CheckBox } from 'react-native-elements';
import { inject, observer } from 'mobx-react';
import { observe } from 'mobx';
import Picker from '~/components/Picker';
import LoadIndicator from '~/components/LoadIndicator';
import { StackActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

@inject('fetchStore', 'dataStore', 'overallStore')
@observer
class NewRequestStack extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      formValue: {

      },
      errors: [],
      loading: false
    }

    observe(this.props.dataStore.createCardStatus, 'status', change => {
      if (change.newValue == 'succes') {
        const { post, id } = this.props.dataStore.new_card;

        this.setState({ loading: false });

        this.props.navigation.dispatch(
          StackActions.replace('RequestStack', { id: id })
        )
      } else {
        Alert.alert(
          "Ошибка",
          this.props.dataStore.errors[0].message,
          [
            { text: "OK" }
          ],
          { cancelable: false }
        );
        this.props.navigation.goBack();
      }
    })
  }

  componentDidMount() {
    const { fetchStore } = this.props;
    this.setState({
      formValue: {
        title: '',
        url: '',
        city_id: 1,
        urgency: 0
      }
    })
    fetchStore.getCitiesList();
  }

  handleChange = (name, value) => {
    this.setState(prevState => ({
      formValue: {
        ...prevState.formValue,
        [name]: value
      }
    }));
  }

  renderTitle = () => {
    const { dataStore } = this.props;
    if (this.state.formValue.title == '') {
      this.handleChange("title", dataStore.getNewCardTitle);
    }
  }

  submitForm = () => {
    this.setState({ loading: true })
    const { title, url, city_id } = this.state.formValue;
    const { navigation, fetchStore, dataStore, } = this.props;

    if (!!title && !!url && !!city_id) {
      dataStore.refreshList();
      dataStore.clearCardsListData();
      fetchStore.createCard(this.state.formValue);
      return
    } else {
      Alert.alert(
        "Ошибка!",
        "Заполнены не все данные!",
        [
          { text: "OK" }
        ],
        { cancelable: false }
      );
      this.props.navigation.goBack();
    }
  }

  render() {
    const { overallStore } = this.props;
    const { formValue, modalVisible, loading } = this.state;

    const size = {
      height: 56,
      margin: {
        marginTop: 16
      }
    }

    return (
      <DismissKeyboard styles={styles.container}>
        {loading ? <LoadIndicator /> :
          <>
            <Input
              fieldName="title"
              onChange={(text) => this.handleChange("title", text)}
              value={formValue.title}
              label="Наименование"
              validate={[]}
              {...size}
            />
            <Input
              fieldName="url"
              onChange={(text) => { this.handleChange("url", text); this.props.fetchStore.getTitle(text); setTimeout(this.renderTitle, 100); }}
              value={formValue.url}
              label="Ссылка"
              validate={[]}
              {...size}
            />
            <Input
              fieldName="city_id"
              multiple={true}
              value={overallStore.getActiveTownNew}
              footerBtn={false}
              search={true}
              fullSize={true}
              headerTitle={"Выбор города"}
              //dataList={this.props.overallStore.getCitiesListNew}
              label="Город"
              onIconPress={() => { this.setState({ modalVisible: true }) }}
              radioButton={true}
              closable={true}
              validate={[]}
              //onChange={(data) => {console.log(data); this.handleChange("city_id", data) }}
              {...size} />
            <View style={styles.checkBoxView}>
              <CheckBox
                uncheckedIcon={<Icon name="checkbox-blank-outline" size={30} color={COLORS.border} />}
                checkedIcon={<Icon name="check-box-outline" size={30} color={COLORS.mainGreen} />}
                containerStyle={{ backgroundColor: 'transparent', borderWidth: 0, left: '-40%' }}
                textStyle={{ fontWeight: '100' }}
                title='Срочно'
                checked={Boolean(formValue.urgency)}
                onPress={() => { this.handleChange("urgency", (formValue.urgency == 0) ? 1 : 0) }}
              />
            </View>
            <Button label="Создать" onPress={() => this.submitForm()} />
            <Picker
              multiple={true}
              footerBtn={false}
              search={true}
              SearchNode={Input}
              onClose={() => this.setState({ modalVisible: false })}
              visible={this.state.modalVisible}
              fullSize={true}
              headerTitle={"Выбор города"}
              dataList={overallStore.getCitiesListNew}
              closable={true}
              radioButton={true}
              onSelectChange={(data) => { overallStore.setTownCheckedNew(data); this.handleChange("city_id", overallStore.getActiveIdTownNew); this.setState({ modalVisible: false }); }} />
          </>
        }
      </DismissKeyboard>
    )
  }
}

export default NewRequestStack;

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.mainBg,
    // alignItems: 'center',
    paddingLeft: marginLeftRight,
    paddingRight: marginLeftRight
  },
  checkBoxView: {
    width: '100%',
    flexDirection: 'row',
    display: 'flex',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    marginBottom: 12,

  }
});
