import React from 'react';
import {
  StyleSheet, View, Text
} from 'react-native';
import { inject, observer } from 'mobx-react';
import Logo from './../../svg/logo.svg';
import { COLORS } from '~/styles';
import DismissKeyboard from '~/components/DismissKeyboard';
import Input from '~/components/Input';
import Button from '~/components/Button';
import { DeviceConst } from './../constants/constants';

@inject('userStore', 'appStore', 'fetchStore')
@observer
class SignIn extends React.Component {

  handleSignIn = () => {
    const {
      login, password,
    } = this.props.userStore;

    this.props.appStore.signIn({ phone: login, password: password });
  }

  render() {
    const {
      setLogin, login, password, setPassword,
    } = this.props.userStore;

    const { hasAuthError, setErrorStatus } = this.props.appStore;

    const size = {
      height: 56,
      margin: {
        marginTop: 16
      }
    }

    return (
      <DismissKeyboard styles={styles.container}>
        <View style={styles.logoView}>
          <Logo style={styles.logo} />
        </View>
        <View style={styles.inputView}>
          <Input
            onChange={setLogin}
            value={login}
            textContentType="telephoneNumber"
            dataDetectorTypes="phoneNumber"
            keyboardType="phone-pad"
            label="Номер телефона"
            hasError={hasAuthError}
            resetError={setErrorStatus}
            secureTextEntry={false}
            maxLength={12}
            {...size}
          />
          <Input
            onChange={setPassword}
            value={password}
            secureTextEntry
            hasError={hasAuthError}
            resetError={setErrorStatus}
            textContentType="password"
            label="Пароль"
            {...size}
          />
          {hasAuthError ?
            <View style={styles.errorBox}>
              <Text style={styles.errorText}>{"Неверный логин или пароль."}</Text>
            </View>
            : null}
          <View style={styles.buttonView}>
            <Button onPress={this.handleSignIn} label="Войти" />
          </View>
        </View>
        <View style={styles.infoView}>
          {/* <Text style={styles.questionText}>Новый пользователь?</Text>
          <TouchableOpacity onPress={() => { RootNavigation.navigate('Login'); }}>
            <Text style={styles.touchableText}>Зарегистрироваться</Text>
          </TouchableOpacity>
          <Text style={styles.questionText}>Забыли пароль?</Text>
          <TouchableOpacity>
            <Text style={styles.touchableText}>Восстановить</Text>
          </TouchableOpacity> */}
        </View>
      </DismissKeyboard>
    );
  }
}

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(4);

const styles = StyleSheet.create({
  errorText: {
    color: COLORS.mainRed,
  },
  errorBox: {
    marginTop: 8,
    //backgroundColor: 'black',
  },
  container: {
    flex: 1,
    backgroundColor: COLORS.mainBg,
  },
  logoView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  logo: {
    aspectRatio: 3 / 1,
  },
  inputView: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    paddingLeft: marginLeftRight,
    paddingRight: marginLeftRight,
    alignContent: 'center',
    justifyContent: 'center'
  },
  buttonView: {
    width: '100%',
    paddingTop: 16
  },
  touchableText: {
    color: COLORS.mainGreen,
    fontSize: 0.045 * DeviceConst.screenWidth,
    margin: 5
  },
  questionText: {
    color: COLORS.default,
    fontSize: 0.045 * DeviceConst.screenWidth,
    margin: 5
  },
  infoView: {
    flex: 0.5,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'center'
  }
});

export default SignIn;
