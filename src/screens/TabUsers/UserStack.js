import * as React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
//import PeriodButton from '~/components/PeriodButton';
import ListRequests from '~/components/ListRequests';
import { COLORS } from '~/styles';
import Filters from '~/components/Filters';
import { STYLES } from '~/styles';
import { inject, observer } from 'mobx-react';
import { DeviceConst } from '~/constants/constants';
@inject('fetchStore', 'dataStore', 'usersTabStore')
@observer
class UserStack extends React.Component {
  constructor(props) {
    super(props);

  }


  componentDidMount() {
    const { fetchStore, usersTabStore } = this.props;
    const { userId } = this.props.route.params;
    usersTabStore.setSelectedUserID(userId);
    usersTabStore.setFilterByUser('all',0);
    fetchStore.getUserCardsListData(userId);
  }

  componentWillUnmount() {
    // const { setUserCards } = this.props.usersTabStore;
    // setUserCards();
        
  }

  render() {

    const { getUserCards, cardFilters, setFilterByUser } = this.props.usersTabStore;

    return (
      <View style={styles.container}>
        <View style={[styles.barStyle, { paddingRight: 6 }]}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Filters filters={cardFilters} onPress={setFilterByUser} />
          </ScrollView>
        </View>
        <ListRequests itemList={getUserCards} />
      </View>
    )
  }
};


const margin = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.mainBg
  },
  barStyle: {
    paddingLeft: margin,
    marginLeft: -8,
  },
});

export default UserStack;