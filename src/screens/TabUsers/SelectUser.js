import * as React from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';
import Button from '~/components/Button';
import Input from '~/components/Input';
import ListUsers from '~/components/ListUsers';
import { inject, observer } from 'mobx-react';
import { DeviceConst } from './../../constants/constants';
import Modal from 'react-native-modal';
import { STYLES } from '~/styles';
const deviceWidth = Dimensions.get("window").width;
const deviceHeight = Dimensions.get("window").height;

@inject('fetchStore', 'usersTabStore')
@observer
class SelectUser extends React.Component {

    constructor(props) {
        super(props);

        this.timer = null;

        this.state = {
            confirmModal: false,
            executor_name: null,
            executor_id: null
        }
    }

    componentDidMount() {
        const { usersTabStore } = this.props;
        usersTabStore.refreshList();
    }

    onSelectUser = ({ userId, userName }) => {
        this.setState({ confirmModal: true, executor_name: userName, executor_id: userId })
    }

    applySelectUser = () => {
        const { fetchStore, navigation, route } = this.props;
        const { card_id } = route.params;
        fetchStore.changeCard(card_id, { executor_id: this.state.executor_id });
        navigation.goBack();
    }

    render() {

        const { getUserListData, setPage, loading, pageCount, page, loadMore, refreshing, setSearchString, filter, refreshList } = this.props.usersTabStore;

        return (
            <View style={styles.container}>
                <View style={styles.searchView}>
                    <Input
                        onChange={(text) => { setSearchString(text); }}
                        value={filter.name}
                        placeholder={"Найти оценщика"} />
                </View>
                <ListUsers
                    topTextLabel="role"
                    middleTextLabel="name"
                    bottomTextLabel="phone"
                    addonAfter={false}
                    currentPage={page}
                    onPressUser={this.onSelectUser}
                    pageCount={pageCount}
                    itemList={getUserListData}
                    loadMore={loadMore}
                    setPage={setPage}
                    refreshing={refreshing}
                    loading={loading}
                    onRefresh={refreshList} />
                <Modal
                    onBackdropPress={() => { this.setState({ confirmModal: false }) }}
                    deviceWidth={deviceWidth}
                    deviceHeight={deviceHeight}
                    isVisible={this.state.confirmModal}
                    style={styles.confirm_modal}
                    onStartShouldSetResponder={() => { return true }}>
                    <View style={styles.confirm} >
                        <Text style={styles.confirm_text}>Выбрать {`${this.state.executor_name}`}?</Text>
                        <View style={styles.confirm_button}>
                            <View style={styles.confirm_button_item}>
                                <Button label="Да" onPress={() => this.applySelectUser()} />
                            </View>
                            <View style={styles.confirm_button_item}>
                                <Button label="Отмена" onPress={() => this.setState({ confirmModal: false })} />
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
};

export default SelectUser;

const padding = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    searchView: {
        padding: padding,
        justifyContent: 'flex-start'
    },
    confirm_modal: {
        padding: 0,
        margin: 10,
        width: deviceWidth - 20,
    },
    confirm: {
        borderRadius: 18,
        backgroundColor: 'white',
        padding: 20,
        alignItems: 'center'
    },
    confirm_text: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    confirm_button: {
        width: '100%',
        flexDirection: 'row',
        marginTop: 20,
        justifyContent: 'space-around'
    },
    confirm_button_item: {
        width: '48%',
    }

});