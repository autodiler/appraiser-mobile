import * as React from 'react';
import { StyleSheet, Alert, ScrollView, View } from 'react-native';
import Input from '~/components/Input';
import Button from '~/components/Button';
import * as RootNavigation from '~/navigation/RootNavigation';
import { COLORS } from '~/styles';
import { DeviceConst } from './../../constants/constants';
import { inject } from 'mobx-react';
import Picker from '~/components/Picker';


const NewUserStack = (props) => {

    const { fetchStore, usersTabStore, navigation } = props;

    const [formValue, setFormValue] = React.useState({})
    const [modalVisible, setModalVisible] = React.useState(false);

    React.useEffect(() => {
        setFormValue({
            name: '',
            phone: '+7',
            email: '',
            role: '',
            password: ''
        });
        fetchStore.getRolesList();
    }, [])

    const handleChange = (name, value) => {
        setFormValue(prevState => ({
            ...prevState,
            [name]: value
        }));
    }

    const submitForm = () => {
        const { name, phone, email, role, password } = formValue;

        if (!!name && !!phone && !!email && !!role && !!password) {
            //usersTabStore.clearUserListData();
            fetchStore.createUser(formValue);
            navigation.goBack()
            return
        }

        return Alert.alert(
            "Ошибка!",
            "Заполнены не все данные!",
            [
                { text: "OK" }
            ],
            { cancelable: false }
        );
    }

    return (
        <ScrollView style={styles.container}>
            <Input
                onChange={(text) => handleChange("name", text)}
                value={formValue.name}
                label="ФИО"
                {...size} />
            <Input
                onChange={(text) => handleChange("phone", text)}
                value={formValue.phone}
                textContentType='telephoneNumber'
                dataDetectorTypes='phoneNumber'
                keyboardType='phone-pad'
                label="Телефон"
                {...size} />
            <Input
                onChange={(text) => handleChange("email", text)}
                value={formValue.email}
                label="E-mail"
                {...size} />
            <Input
                multiple={true}
                value={formValue.role}

                label="Роль"
                onIconPress={() => { setModalVisible(true) }}
                closable={true}
                onChange={(data) => { handleChange("role", data.join(', ')) }}
                {...size} />
            <Input
                onChange={(text) => handleChange("password", text)}
                value={formValue.password}
                secureTextEntry
                textContentType="password"
                label="Пароль"
                {...size}
            />
            <Picker
                multiple={true}
                dataList={usersTabStore.getRolesList}
                onClose={() => setModalVisible(false)}
                visible={modalVisible}
                closable={true}
                onChange={(data) => handleChange("role", data.join(', '))}
                onSelectChange={(data) => { overallStore.setTownChecked(data) }} />
            <View style={styles.buttonView}>
                <Button label="Добавить" onPress={() => { submitForm() }} />
            </View>
        </ScrollView>
    )
};

const marginLeftRight = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const size = {
    height: 56,
    margin: {
        marginTop: 16
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.mainBg,
        paddingLeft: marginLeftRight,
        paddingRight: marginLeftRight
    },
    buttonView: {
        width: '100%',
        paddingTop: 16
    },
});

export default inject('fetchStore', 'usersTabStore')(NewUserStack);