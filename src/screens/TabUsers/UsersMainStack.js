import * as React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import Input from '~/components/Input';
import ListUsers from '~/components/ListUsers';
import AddButton from '~/components/AddButton';
import * as RootNavigation from '~/navigation/RootNavigation';
import { STYLES } from '~/styles';
import { inject, observer } from 'mobx-react';
import SafeAreaHooks from '~/components/SafeAreaHooks';
import { DeviceConst } from './../../constants/constants';
import Filters from '~/components/Filters';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
@inject('fetchStore', 'usersTabStore')
@observer
class UsersMainStack extends React.Component {

   constructor(props) {
      super(props);

      this.timer = null;
   }

   componentDidMount() {
      const { usersTabStore } = this.props;
      usersTabStore.refreshList();
   }

   render() {

      const { getUserListData, setPage, loading, pageCount, page, loadMore, refreshing, setFilterStatus, setSearchString, filter, filters, refreshList } = this.props.usersTabStore;
      const size = {
         height: 56,
      }

      return (
         <>
            <View style={STYLES.barStyle}>
               <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                  <Filters filters={filters} onPress={setFilterStatus} />
               </ScrollView>
            </View>
            <View style={styles.searchView}>
               <Input
                  onChange={(text) => { setSearchString(text); }}
                  value={filter.name}
                  placeholder={"Найти пользователя"}
                  {...size} />
            </View>
            <ListUsers
               topTextLabel="role"
               middleTextLabel="name"
               bottomTextLabel="phone"
               currentPage={page}
               pageCount={pageCount}
               addonAfter={true}
               itemList={getUserListData}
               loadMore={loadMore}
               setPage={setPage}
               refreshing={refreshing}
               loading={loading}
               onRefresh={refreshList} />
            <AddButton
               icon={
                  <Icon
                     name="account"
                     size={24}
                     color="white"

                  />}
               onPress={() => { RootNavigation.navigate('NewUserStack') }}
               label="+" />
         </>
      )
   }
};

export default SafeAreaHooks(UsersMainStack);

const padding = DeviceConst.pixelRatio.getPixelSizeForLayoutSize(6);

const styles = StyleSheet.create({
   searchView: {
      padding: padding,
      // justifyContent: 'flex-start'
   }
});