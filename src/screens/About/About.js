import React from 'react';
import {
    StyleSheet, View, Text,
} from 'react-native';
import Constants from 'expo-constants';
import { Platform } from 'react-native';

import Logo from './../../../svg/logo.svg';

class About extends React.Component {
    constructor() {
        super();

    }

    render() {
        const version = Platform.OS == 'android' ? Constants.platform.android.versionCode : Constants.platform.ios.buildNumber;
        return (

            <View style={styles.logoView}>
                <Logo style={styles.logo} />
                <Text style={styles.infoText}>{`Версия: ${version}`}</Text>
            </View>

        );
    }
}
const styles = StyleSheet.create(
    {
        logoView: {
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1
        },
        logo: {
            marginTop: 40
        },
        infoText: {
            margin: 20,
            fontSize: 16
        }
    });

export default About;