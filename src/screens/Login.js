import React from 'react';
import { View, StyleSheet } from 'react-native';
import Input from '~/components/Input';
import Button from '~/components/Button';
import { inject } from 'mobx-react';
import { COLORS } from '~/styles';

@inject('userStore')
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      fullName: '',
      number: '+7',
      email: ''
    };
  }

  render() {
    const {
      name, fullName, number, email
    } = this.state;

    return (
      <View style={styles.container}>
        <Input
          onChange={(text) => { this.setState({ name: text }); }}
          value={name}
          label="Имя"
        />
        <Input
          onChange={(text) => { this.setState({ fullName: text }); }}
          val={fullName}
          label="Фамилия"
        />
        <Input
          onChange={(text) => { this.setState({ number: text }); }}
          value={number}
          textContentType="telephoneNumber"
          dataDetectorTypes="phoneNumber"
          keyboardType="phone-pad"
          label="Телефон"
        />
        <Input
          onChange={(text) => { this.setState({ email: text }); }}
          val={email}
          label="e-mail"
        />
        <Button label="Добавить" onPress={() => {  }} />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    backgroundColor: COLORS.mainBg,
  }
});

export default Login;
