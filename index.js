import { AppRegistry } from 'react-native';
import App from './App';
import { enableScreens } from 'react-native-screens';
enableScreens();
import { name as appName } from './app.json';
import * as Sentry from 'sentry-expo';

Sentry.init({
    dsn: 'https://f34a6a6db2674232b3dd9ea84154368a@o487524.ingest.sentry.io/5546399',
    enableInExpoDevelopment: true,
    debug: true,
  });
  

AppRegistry.registerComponent(appName, () => App);