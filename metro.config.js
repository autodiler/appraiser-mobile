
/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */
const { getDefaultConfig } = require("metro-config");
const path = require('path');
// const blacklist = require('metro').createBlacklist;
const blacklist = require('metro-config/src/defaults/blacklist');

module.exports = (async () => {
    const {
        resolver: { sourceExts, assetExts }
    } = await getDefaultConfig();

    return {
        transformer: {
            getTransformOptions: async () => ({
                transform: {
                    experimentalImportSupport: false,
                    inlineRequires: false,
                },
            }),
            babelTransformerPath: require.resolve("react-native-svg-transformer")
        },
        resolver: {
            assetExts: assetExts.filter(ext => ext !== "svg"),
            sourceExts: [...sourceExts, "svg"],
            extraNodeModules: {
                "react-native-sdk": path.resolve(__dirname, "node_modules/react-native-sdk")
            },
            blacklistRE: blacklist([
                /node_modules\/.*\/node_modules\/react-native\/.*/,
                /nodejs-assets\/.*/,
                /android\/.*/,
                /ios\/.*/
            ])
        },
        projectRoot: path.resolve(__dirname),
        watchFolders: [
            path.resolve(__dirname, "node_modules/react-native-sdk"),
        ]
    };
})();