#!/bin/bash
# I had to specify DEBIAN_FRONTEND=noninteractive as we get after install apt-utils: debconf: unable to initialize frontend: Dialog
# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && [[ ! -e /.dockerinit ]] && exit 0

set -xe

# add the add-apt-repository command
#DEBIAN_FRONTEND=noninteractive apt-get install software-properties-common -y

# Install git (the php image doesn't have it) which is required by composer
#apt-get update -yqq
#debconf: delaying package configuration, since apt-utils is not installed
#apt-get install git -yqq
#DEBIAN_FRONTEND=noninteractive apt-get install apt-utils git libzip-dev zip unzip -y
#DEBIAN_FRONTEND=noninteractive apt-get install -y libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
#    libfreetype6-dev
# Here you can install any other extension that you need
#docker-php-ext-install pdo_mysql intl mcrypt soap

#docker-php-ext-configure gd --with-gd --with-webp-dir --with-jpeg-dir \
#    --with-png-dir --with-zlib-dir --with-xpm-dir --with-freetype-dir

#docker-php-ext-install bcmath zip gd