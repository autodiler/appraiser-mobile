FROM node:latest

WORKDIR /var/www/src
VOLUME /var/www/src

RUN export NODE_OPTIONS=--max_old_space_size=4096

RUN npm -g config set user root
# RUN npm install -g expo-cli
# RUN npm install

EXPOSE 19000
EXPOSE 19001
EXPOSE 19002

CMD ["expo", "start", "--no-dev", "--minify", "--offline", "--non-interactive", "--tunnel"]