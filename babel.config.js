module.exports = function (api) {
  api.cache(true);

  const rootImportOpts = {
    paths: [
      {
        root: __dirname,
        rootPathPrefix: '~/',
        rootPathSuffix: 'src',
      },
    ]
  };

  return {
    presets: ['babel-preset-expo'],
    plugins: [
      [
        "@babel/plugin-proposal-decorators",
        {
          "legacy": true
        }
      ],
      ['babel-plugin-root-import', rootImportOpts]
    ],
  };
};
